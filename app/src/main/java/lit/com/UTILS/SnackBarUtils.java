package lit.com.UTILS;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Manay.Khan on 11/10/2016.
 */
public class SnackBarUtils {
    public static void showSnackBar(View rootView, String text, int duration){

        Snackbar snack = Snackbar.make(rootView,text, Snackbar.LENGTH_SHORT);
        View view = snack.getView();
        view.setBackgroundColor(Color.BLACK);
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.parseColor("#ffffff"));
        snack.show();
    }

}

