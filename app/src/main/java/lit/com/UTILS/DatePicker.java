package lit.com.UTILS;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DatePicker {

    Activity activity;
    public SimpleDateFormat dateFormatter, day, dateFormatterServices;
    public DatePickerDialog fromDatePickerDialog;
    Calendar newDate;
    public static String date;

    public DatePicker(Activity activity) {
        this.activity = activity;
    }

    @SuppressLint("SimpleDateFormat")
    public void datepicker(final TextView tv) {
        //  dateFormatter = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.US);
        dateFormatter = new SimpleDateFormat("dd MMMM yyyy");

        //day = new SimpleDateFormat("EEEE");

        dateFormatterServices = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();


        fromDatePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                tv.setText(dateFormatter.format(newDate.getTime()));
                date = dateFormatterServices.format(newDate.getTime());
                Log.v("date: ", date);

            }


        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        fromDatePickerDialog.show();


    }


}
