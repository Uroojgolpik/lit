package lit.com.UTILS;

/**
 * Created by Urooj on 3/9/2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import lal.adhish.gifprogressbar.GifView;
import lit.com.R;

@SuppressLint("Registered")
public class DialogBoxes extends Activity {
    Dialog progressDialog;
    Activity activity;
    ImageView im;
    AlertDialog alertDialog;
    static AlertDialog alertDialog1;

    public DialogBoxes(Activity activity) {
        this.activity = activity;
    }

    public void alertDailog (final Activity activity, String title){
        alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setMessage(title);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                activity.finishAffinity();
            }
        });

        alertDialog.show();
    }

    public static void alterDialog (final Activity activity, final String text){
        alertDialog1 = new AlertDialog.Builder(activity).create();
        alertDialog1.setMessage(text);



        alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String message = text;
                alertDialog1.dismiss();
            }
        });



        alertDialog1.show();
    }
    public void  showProgress(){

        progressDialog = new Dialog(activity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progressbar);

        GifView pGif = (GifView) progressDialog.findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader1);
        progressDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void  hideProgress(){

        progressDialog.dismiss();

    }
}
