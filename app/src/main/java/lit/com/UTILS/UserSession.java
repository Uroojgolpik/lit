package lit.com.UTILS;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;


/**
 * Created by urooj.khalid on 12/21/2016.
 */

public class UserSession {

    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;
    //firstName, lastName, userEmail, userId, userRoleId, gender, apiToken, token, state, dob, company, isVerified

    public static final String PREF_NAME = "UserSession";
    public static final String ID = "user_id";
    public static final String USERNAME = "UserName";
    public static final String EMAIL = "USER_email";

    public static final String PHONE = "user_contact";
    public static final String STATE = "user_country";
    public static final String COMPANY = "user_city";
    public static final String IMAGE = "user_image";
    public static final String APITOKEN = "AccessToken";

    public static final String LONGITUDE = "user_latitude";
    public static final String LATITUDE = "user_longitude";
    public static final String DEVICE_TOKEN = "user_device";
    public static final String ROLEID = "user_role_id";
    public static final String GENDER = "user_gender";
    public static final String DOB = "user_dob";
    public static final String TOKEN = "user_TOKEN";
    public static final String VERIFIED = "user_VERIFIED";
    public static final String EXPERIENCE = "user_exp";
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String LIST = "saveList";
    public static final String INSTITUTE = "user";



    // Constructor
    public UserSession(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

   // firstName, lastName, userEmail, userId, userRoleId, gender, apiToken, token, state, dob, company, isVerified
    public void createSession(String firstName, String lastName, String userEmail,
                              String userId, String userRoleId, String gender,
                              String apiToken, String token, String state, String dob,
                              String company, String isVerified){

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(ID, userId);
        editor.putString(USERNAME, firstName+" "+lastName);
        editor.putString(EMAIL, userEmail);
        editor.putString(COMPANY, company);
        editor.putString(APITOKEN, apiToken);
        editor.putString(TOKEN, token);
        editor.putString(ROLEID, userRoleId);
        editor.putString(STATE, state);
        editor.putString(DOB, dob);
        editor.putString(GENDER, gender);
        editor.putString(VERIFIED, isVerified);
        editor.commit();

    }

    public void createSessionafterUpdate(String teacher_name, String teacher_email, String teacher_id,
                                         String teacher_image, String access, String type,
                                         String video, String country, String institute, String exp, String skills,
                                         String desp, String qua, String phone){

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(ID, teacher_id);
        editor.putString(USERNAME, teacher_name);
        editor.putString(EMAIL, teacher_email);
        editor.putString(IMAGE, teacher_image);

        editor.putString(APITOKEN, access);
        editor.putString(ROLEID, video);
        editor.putString(STATE, country);
        editor.putString(TOKEN, qua);
        editor.putString(EXPERIENCE, exp);
        editor.putString(VERIFIED, skills);
        editor.putString(DOB, desp);
        editor.putString(INSTITUTE, institute);
        editor.putString(PHONE, phone);
        editor.commit();

    }
    public void uploadedImageURL(String image){

        editor.putString(IMAGE,image);

        editor.commit();

//        Intent i = new Intent(_context, MasterActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        _context.startActivity(i);

    }

    public void updatedProfile(String contact, String city_id, String country_id ){

        editor.putString(PHONE, contact);
        editor.putString(STATE, country_id);
        editor.putString(COMPANY, city_id);


        editor.commit();


    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){

            return true;
        }

        return false;
    }


    public void setUserLatLon(String latitude, String longitude){

        editor.putString(LATITUDE, latitude);
        editor.putString(LONGITUDE, longitude);

        editor.commit();
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name

        user.put(ID,pref.getString(ID,null));
        user.put(USERNAME,pref.getString(USERNAME,null));
        user.put(EMAIL, pref.getString(EMAIL, null));
        user.put(COMPANY, pref.getString(COMPANY, null));
        user.put(PHONE,pref.getString(PHONE,null));
        user.put(STATE,pref.getString(STATE,null));
        user.put(IMAGE,pref.getString(IMAGE,null));
        user.put(DEVICE_TOKEN,pref.getString(DEVICE_TOKEN,null));
        user.put(APITOKEN,pref.getString(APITOKEN,null));
        user.put(ROLEID,pref.getString(ROLEID,null));
        user.put(GENDER,pref.getString(GENDER,null));
        user.put(TOKEN,pref.getString(TOKEN,null));
        user.put(EXPERIENCE,pref.getString(EXPERIENCE,null));
        user.put(VERIFIED,pref.getString(VERIFIED,null));
        user.put(DOB,pref.getString(DOB,null));
        user.put(LIST,pref.getString(LIST, null));
        user.put(INSTITUTE,pref.getString(INSTITUTE,null));
        return user;
    }

    public HashMap<String, String> getList(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name

        user.put(LIST,pref.getString(LIST, null));

        return user;
    }

    public HashMap<String, String> getId(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(ID,pref.getString(ID,null));
        user.put(APITOKEN, pref.getString(APITOKEN, null));
        return user;
    }


    /**
     * Clear session details
     * */
    public void logoutUser(){
        editor.clear();
        editor.commit();
    }


    public void saveList(String list){

        editor.putString(LIST, list);
        editor.commit();

    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }



}
