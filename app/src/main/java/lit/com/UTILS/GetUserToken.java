package lit.com.UTILS;

import android.app.Activity;
import android.util.Log;

import java.util.HashMap;

public class GetUserToken {


    public static String getUserSession(Activity activity){

        UserSession userSession = new UserSession(activity);
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();

        String token = userSessionDetails.get(UserSession.APITOKEN);

        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.TOKEN)));
        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));

        return token;
    }
}

