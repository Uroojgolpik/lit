package lit.com.UTILS;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.widget.EditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by urooj.khalid on 12/20/2016.
 */

public class Validations {
    Context context;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static final int REQUEST_FINE_LOCATION = 2;
    private static final String[] PERMISSIONS__FINE={
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    public static boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean cvvValidator(String cvv)
    {
        Pattern pattern;
        Matcher matcher;
        final String CVV_PATTERN = "/^[0-9]{3,4}$/";
        pattern = Pattern.compile(CVV_PATTERN);
        matcher = pattern.matcher(cvv);
        return matcher.matches();

    }

    public static boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 8) {
            return true;
        }
        return false;
    }

    public static boolean name(String name) {
        if (!name.isEmpty()){
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean number(String number) {
        if (number != null && number.length() <= 16){
            return true;
        }
        return false;
    }

    public static boolean webSite (String webSite) {
        if (webSite != null && webSite.length() > 6) {
            return true;
        }
        return false;
    }

    public static boolean location(String location) {
        if (location != null && location.length() > 6) {
            return true;
        }
        return false;
    }

    public boolean company(String company) {
        if (company != null && company.length() > 2) {
            return true;
        }
        return false;
    }

    public static boolean isValidLogin(String password) {
        if (password != null) {
            return true;
        }
        return false;
    }

    public static boolean gender(String gender) {
        if (gender != null) {
            return true;
        }
        return false;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean verifyLocationPermissions(Activity activity) {
        int finepermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (finepermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS__FINE,
                    REQUEST_FINE_LOCATION
            );
              return true;
        }
       return  false;
    }

    public static void multiplePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private static void recursivefun(int n) {

        if(n <= 150) {

           // age_p.add(String.valueOf(n));
            System.out.println(n);
            recursivefun(n+1);


        }
    }

    public static boolean firstLastName(String name){
        Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
        Matcher ms = ps.matcher(name);
        boolean bs = ms.matches();
        return bs;
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
          //  Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File compressImage(String imagePath) {
        Bitmap bmp = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, bos);

       // SessionFileList newFIle = saveBitmap(bmp, imagePath);
        return saveBitmap(bmp, imagePath);
    }

    public static File saveBitmap(Bitmap bitmap, String path) {
        File file = null;
        if (bitmap != null) {
            file = new File(path);
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }


//    private void checkMultipleRunTimePermission() {
//        int permissionReadStorage = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//
//        List<String> permissionsNeeded = new ArrayList<>();
//        if (permissionReadStorage != PackageManager.PERMISSION_GRANTED) {
//            permissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
//        }
//        if (!permissionsNeeded.isEmpty()) {
//            ActivityCompat.requestPermissions(this,
//                    permissionsNeeded.toArray(new String[permissionsNeeded.size()]), 124);
//        }
//    }
//
//    private void checkTimePermission() {
//        int permissionReadStorage = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//        List<String> permissionsNeeded = new ArrayList<>();
//        if (permissionReadStorage != PackageManager.PERMISSION_GRANTED) {
//            permissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        }
//        if (!permissionsNeeded.isEmpty()) {
//            ActivityCompat.requestPermissions(this,
//                    permissionsNeeded.toArray(new String[permissionsNeeded.size()]), 124);
//        }
//    }

}
