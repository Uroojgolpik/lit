package lit.com.UTILS;

import android.app.Activity;
import android.widget.TextView;

import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.R;

public class Constants {

    public static int tax = 12;
    LITDB litdb;


    public static void counter(Activity activity, int count){

        TextView tv = (TextView) activity.findViewById(R.id.toolbar_cart_counter);
        tv.setText(count+"");
    }

    public static void loginStatus(Activity activity, String status){


        TextView tv = (TextView) activity.findViewById(R.id.tv_login_logout);
        if (status.equals("LOGIN")){
            tv.setText("LOGIN");
        }
        if (status.equals("LOGOUT")){
            tv.setText("LOGOUT");
        }
    }
}
