package lit.com.APIs;

import lit.com.Response.CheckoutServiceResponse;
import lit.com.Response.CountryResponse;
import lit.com.Response.ProfileViewResponse;
import lit.com.Response.LoginServiceResponse;
import lit.com.Response.LogoutResponse;
import lit.com.Response.MyOrdersServicesResponse;
import lit.com.Response.NearByDetailServicesResponse;
import lit.com.Response.NearByServicesResponse;
import lit.com.Response.SearchTestResponse;
import lit.com.Response.SignupServiceResponse;
import lit.com.Response.StateServicesResponse;
import lit.com.Response.TestBundlesDetailsServicesResponse;
import lit.com.Response.TestBundlesServicesResponse;
import lit.com.Response.TestDetailsServicesResponse;
import lit.com.Response.TestServiceResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Cc on 1/3/2018.
 */

public interface LITServices {

    String Base_URL = "http://golpik.net/projects/laravel/optimalhealth/api/";


    //Login Service
    @FormUrlEncoded
    @POST("login")
    Call<LoginServiceResponse> loginService(@Field("email") String email, @Field("password") String password);


    //SignUp Service
    @FormUrlEncoded
    @POST("register")
    Call<SignupServiceResponse> signUpService(@Field("firstName") String firstName, @Field("lastName") String lastName,
                                              @Field("email") String email, @Field("password") String password,
                                              @Field("password_confirmation") String password_confirmation, @Field("gender") String gender,
                                              @Field("state") String state, @Field("date") String date,
                                              @Field("month") String month, @Field("year") String year,
                                              @Field("city") String city, @Field("zip") String zip,
                                              @Field("address") String address, @Field("phone") String phone);

    //State Service
    @GET("State")
    Call<StateServicesResponse> getStates();


    @GET("Countries")
    Call<CountryResponse> getCountry();


    //Tests Service
    @GET("products")
    Call<TestServiceResponse> getAllTests();


    //Tests Details Service
    @GET("product/{id}")
    Call<TestDetailsServicesResponse> getTestDetails(@Path("id") String id);


    //Tests Bundles Service
    @GET("bundles")
    Call<TestBundlesServicesResponse> getTestBundles();


    //Tests Bundles Details Service
    @GET("bundles/{id}")
    Call<TestBundlesDetailsServicesResponse> getBundlesDetails(@Path("id") String id);


    //NearBYLocation Service
    @FormUrlEncoded
    @POST("locations")
    Call<NearByServicesResponse> getAllNearByLocations(@Field("zip") String zip, @Field("city") String city,
                                               @Field("state") String state);

//    @FormUrlEncoded
//    @POST("location")
//    Call<NearByServicesResponse> getAllNearByLocations(@Field("zip") String zip, @Field("city") String city,
//                                                       @Field("state") String state);

    // Location Details
    @GET("location/{id}")
    Call<NearByDetailServicesResponse> getNearByLocationDetails(@Path("id") String id);



    // My Orders
   // @FormUrlEncoded
    @GET("myorders/{id}")
    Call<MyOrdersServicesResponse> getMyOrders(@Path("id") String token);


    // Logout Service
    @FormUrlEncoded
    @POST("logout")
    Call<LogoutResponse> logout(@Field("token") String token);


    // Profile Service
    @GET("profile/{token}")
    Call<ProfileViewResponse> getUserProfile(@Path("token") String token);


//    token:2jGjB1klOAL3QTQRr4nGzRr1AOQhByKIi82jSrsy9psancfjSsZunvqQbCxP
//    firstName:testu
//    lastName:test
//    email:aoo@gmail.com
//    country_id:7
//    state:CA
//    city:yyyyyy
//    address1:gggghhh
//    zip:12
//    phone:""
//    gender:Female
//    cc:4242424242424242
//    expMonth:3
//    expYear:2021
//    cvc:1324
//    product_id:1,2
//    price:39
////message:null
//    grandTotal:474
//    address1:gggghhh
////address2:newyork
//    year:2018
//    month:09
//    date:28

    @FormUrlEncoded
    @POST("place_order")
    Call<CheckoutServiceResponse> checkout(@Field("token") String token, @Field("firstName") String firstName,
                                           @Field("lastName") String lastName, @Field("email") String email,
                                           @Field("country_id") String country_id, @Field("state") String state,
                                           @Field("city") String city, @Field("address1") String address1,
                                           @Field("zip") String zip, @Field("phone") String phone,
                                           @Field("gender") String gender, @Field("cc") String cc,
                                           @Field("expMonth") String expMonth, @Field("expYear") String expYear,
                                           @Field("cvc") String cvc, @Field("product_id") String product_id,
                                           @Field("price") String price, @Field("message") String message,
                                           @Field("grandTotal") String grandTotal, @Field("address1") String address,
                                           @Field("address2") String address2, @Field("year") String year,
                                           @Field("month") String month, @Field("date") String date);

    @FormUrlEncoded
    @POST("searchproduct")
    Call<SearchTestResponse> searchTests(@Field("keyword") String keyword);

}
