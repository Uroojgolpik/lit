package lit.com.LatestFragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterNearByLabs;
import lit.com.Adapter.SpinnerAdapter;
import lit.com.R;
import lit.com.Response.NearByServicesData;
import lit.com.Response.NearByServicesResponse;
import lit.com.Response.StateServicesData;
import lit.com.Response.StateServicesResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNearBySearchLabs extends Fragment{

    View view;
    ListView listView;
    List<NearByServicesData> dataList = new ArrayList<>();
    AdapterNearByLabs adapter ;
    LITServices services;
    EditText ed_search_loc_city, ed_search_loc_zipcode;
    Spinner spinner_search_loc_states;
    LinearLayout  ll_search_location;
    Bundle bundle ;
    String city, state, zipcode;
    DialogBoxes dialogBoxes;
    List<StateServicesData> dataListState = new ArrayList<StateServicesData>();
    ImageButton ib_search_locations;
    Spinner sp_state;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.framgent_locations, container, false);
        linkXML();
        return view;
    }

    private void linkXML() {

        sp_state = (Spinner) view.findViewById(R.id.spinner_search_loc_states);
        ed_search_loc_city = (EditText) view.findViewById(R.id.ed_search_loc_city);
        ed_search_loc_zipcode = (EditText) view.findViewById(R.id.ed_search_loc_zipcode);
        ll_search_location = (LinearLayout) view.findViewById(R.id.ll_search_location);
        ib_search_locations = (ImageButton) view.findViewById(R.id.ib_search_locations);


        if (dataListState.size()<=0){
            getStateList();
        }
        else {
            if (dataListState.size()>0) {
                SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, dataListState);
                sp_state.setAdapter(spinnerAdapter);
            }
        }

      onClickListner();

    }
    private void onClickListner() {

        ll_search_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                city = ed_search_loc_city.getText().toString();
                zipcode = ed_search_loc_zipcode.getText().toString();

//                city = "";
//                zipcode = "";
//                state = "";

                bundle = new Bundle();
                bundle.putString("state", state);
                bundle.putString("city", city);
                bundle.putString("zipcode", zipcode);
                Fragment fragment = new FragmentNearByLabs();
                fragment.setArguments(bundle);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment, fragment, "FragmentNearByLabs")
                        .addToBackStack("FragmentNearByLabs")
                        .commitAllowingStateLoss();
                //  }
            }
        });
        ib_search_locations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    state = String.valueOf(dataListState.get(sp_state.getSelectedItemPosition()-1).getCode());
                }
                if (view != null) {
                    ((TextView) view).setTextColor(Color.BLACK);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getStateList() {
        final StateServicesData stateServicesData = new StateServicesData("Select A State", "");
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<StateServicesResponse> getStates = services.getStates();
        getStates.enqueue(new Callback<StateServicesResponse>() {
            @Override
            public void onResponse(Call<StateServicesResponse> call, Response<StateServicesResponse> response) {
                Gson gson = new Gson();
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataListState.add(stateServicesData);
                        dataListState.addAll(response.body().getData());
                        if (dataListState.size()>0) {
                            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),
                                    android.R.layout.simple_dropdown_item_1line, dataListState);
                            sp_state.setAdapter(spinnerAdapter);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<StateServicesResponse> call, Throwable t) {

            }
        });
    }

}
