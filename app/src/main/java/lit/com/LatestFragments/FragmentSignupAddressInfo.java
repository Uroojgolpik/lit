package lit.com.LatestFragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.SpinnerAdapter;
import lit.com.R;
import lit.com.Response.SignupServiceResponse;
import lit.com.Response.StateServicesData;
import lit.com.Response.StateServicesResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import lit.com.UTILS.Validations;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSignupAddressInfo extends Fragment  {


    View view;
    EditText  edt_city, edt_phone, edt_zip_code, edt_address;
    Spinner sp_state;
    String email, firstName, lastName,password, dob, gender, state, city, phone, zip, address;
    String date [];
    LinearLayout ll_done, ll_back;
    LITServices services;
    Button btn_signup_now;
    Bundle bundle ;
    DialogBoxes dialogBoxes;
    AlertDialog alertDialog1;

    List<StateServicesData> dataList = new ArrayList<StateServicesData>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        firstName= bundle.getString("fn");
        lastName= bundle.getString("ln");
        email = bundle.getString("email" );
        password = bundle.getString("password");
        gender = bundle.getString("gender" );
        dob = bundle.getString("dob");

        date = dob.split("-");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedtInstanceState) {
        view = inflater.inflate(R.layout.fragment_signup_next, container, false);
        linkXML(view);

        return view;
    }

    private void linkXML(View view) {

        edt_city = (EditText) view.findViewById(R.id.signup_edt_city);
        edt_phone = (EditText) view.findViewById(R.id.signup_edt_phone);
        sp_state = (Spinner) view.findViewById(R.id.signup_sp_state);
        edt_zip_code = (EditText) view.findViewById(R.id.signup_edt_postal_code);
        edt_address = (EditText) view.findViewById(R.id.signup_edt_address);
        btn_signup_now = (Button) view.findViewById(R.id.btn_signup_now);
//        ll_done = (LinearLayout) view.findViewById(R.id.signup_ll_done);
//        ll_back = (LinearLayout) view.findViewById(R.id.signup_ll_back);

        getList();
        onClickListner();
    }

    private void onClickListner() {

        btn_signup_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyAll();
            }
        });
//        ll_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getFragmentManager().popBackStack();
//            }
//        });

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    state = String.valueOf(dataList.get(sp_state.getSelectedItemPosition()-1).getCode());
                }
                else {
                    //   DialogBoxes.alterDialog(this, "");
                }
                ((TextView) view).setTextColor(Color.BLACK);
//                String state = (String) adapterView.getItemAtPosition(position);
//                ((TextView) adapterView.getChildAt(0)).setTextColor(R.color.colorWhite);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void verifyAll() {

        state = sp_state.getSelectedItem().toString();
        city = edt_city.getText().toString();
        phone = edt_phone.getText().toString();
        zip = edt_zip_code.getText().toString();
        address = edt_address.getText().toString();

        if (TextUtils.isEmpty(state)){
            DialogBoxes.alterDialog(getActivity(), "Select Any State");
        }
        else if (TextUtils.isEmpty(city)){
            edt_city.setError("Enter Your City");
        }
        else if (!Validations.number(phone)){
            edt_phone.setError("Enter Your Phone Number");
        }

        else if (TextUtils.isEmpty(zip)){
            edt_zip_code.setError("Enter Your Postal Code");
        }

        else if (TextUtils.isEmpty(address)){
            edt_address.setError("Enter Your ProfileServiceDataAddress");
        }
        else {
           executeService();
        }


    }

    private void executeService() {
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<SignupServiceResponse> executeService = services.signUpService(firstName, lastName, email, password, password,gender,
                state, date[0], date[1], date[2], city, zip, address, phone);
        executeService.enqueue(new Callback<SignupServiceResponse>() {
            @Override
            public void onResponse(Call<SignupServiceResponse> call, Response<SignupServiceResponse> response) {
                if (response.body().getStatus().equals(1)){
                    if (response.body().getData() != null){
                        dialogBoxes.hideProgress();
                        alterDialog(response.body().getMessage());
                    }
                }
                else {
                    dialogBoxes.hideProgress();
                    DialogBoxes.alterDialog(getActivity(), response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<SignupServiceResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);

                dialogBoxes.hideProgress();

            }
        });


    }

    public void alterDialog (final String text){
        alertDialog1 = new AlertDialog.Builder(getActivity()).create();
        alertDialog1.setMessage(text);



        alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog1.dismiss();
                getFragmentManager().popBackStack();
            }
        });



        alertDialog1.show();
    }

    private void getList() {
        final StateServicesData stateServicesData = new StateServicesData("Select A State", "");
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<StateServicesResponse> getStates = services.getStates();
        getStates.enqueue(new Callback<StateServicesResponse>() {
            @Override
            public void onResponse(Call<StateServicesResponse> call, Response<StateServicesResponse> response) {
                Gson gson = new Gson();
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataList.add(stateServicesData);
                        dataList.addAll(response.body().getData());
                        if (dataList.size()>0) {
                            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, dataList);
                            sp_state.setAdapter(spinnerAdapter);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<StateServicesResponse> call, Throwable t) {

            }
        });
    }


}
