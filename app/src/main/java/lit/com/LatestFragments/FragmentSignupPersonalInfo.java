package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import lit.com.R;
import lit.com.UTILS.DatePicker;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.Validations;


public class FragmentSignupPersonalInfo extends Fragment {


    View view;
    EditText edt_email, edt_firstName, edt_lastName, edt_password, edt_confirmPassword;
    String email, firstName, lastName,password,confirmPassword, date = null, gender=null;
    RadioButton rb_male, rb_female;
    TextView tv_dob;
    Button ll_next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedtInstanceState) {
        view = inflater.inflate(R.layout.fragment_signup, container, false);
        linkXML(view);
        return view;
    }

    private void linkXML(View view) {
        
        edt_firstName = (EditText) view.findViewById(R.id.signup_edt_fn);
        edt_lastName = (EditText) view.findViewById(R.id.signup_edt_ln);
        edt_email = (EditText) view.findViewById(R.id.signup_edt_email);
        edt_password = (EditText) view.findViewById(R.id.signup_edt_pass);
        edt_confirmPassword = (EditText) view.findViewById(R.id.signup_edt_cpass);
        tv_dob = (TextView) view.findViewById(R.id.signup_tv_dob);
        rb_male = (RadioButton) view.findViewById(R.id.signup_rb_male);
        rb_female = (RadioButton) view.findViewById(R.id.signup_rb_female);
        ll_next = (Button) view.findViewById(R.id.signup_btn_next);

        onClickListner();
    }

    private void onClickListner() {

        tv_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker datePicker = new DatePicker(getActivity());
                datePicker.datepicker(tv_dob);

            }
        });
        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyAll();
            }
        });
    }

    private void verifyAll() {

//        edt_email.setText("test@yopmail.com");
//        edt_firstName.setText("test");
//        edt_lastName.setText("test");
//        edt_password.setText("12345678");
//        edt_confirmPassword.setText("12345678");


        email = edt_email.getText().toString();
        firstName = edt_firstName.getText().toString();
        lastName = edt_lastName.getText().toString();
        password = edt_password.getText().toString();
        confirmPassword = edt_confirmPassword.getText().toString();

        if (rb_male.isChecked()){
            gender = rb_male.getText().toString();

        }
        if (rb_female.isChecked()){
            gender = rb_female.getText().toString();

        }

        if (!Validations.firstLastName(firstName)){
            edt_firstName.setError("Enter Your First Name");
        }
        else if (!Validations.firstLastName(lastName)){
            edt_lastName.setError("Enter Your Last Name");
        }
        else if (!Validations.isEmailValid(email)){
            edt_email.setError("Enter Your Email ProfileServiceDataAddress");
        }
        else if (!Validations.isValidPassword(password)){
            edt_password.setError("Enter Your Password");
        }
        else if (!confirmPassword.equals(password)){
            edt_confirmPassword.setError("Password not matched");
        }

        else if (TextUtils.isEmpty(tv_dob.getText().toString())){
            DialogBoxes.alterDialog(getActivity(), "Select Date Of Birth");
        }
        else if (TextUtils.isEmpty(gender)){
            DialogBoxes.alterDialog(getActivity(), "Select Gender");
        }
        else {
            date = DatePicker.date;
            Log.v("date: ", date+"");
            Log.v("date: ", DatePicker.date+"");
            Bundle bundle = new Bundle();
            Fragment fragment = new FragmentSignupAddressInfo();

            bundle.putString("fn", firstName);
            bundle.putString("ln", lastName);
            bundle.putString("email", email);
            bundle.putString("password", password);
            bundle.putString("gender", gender);
            bundle.putString("dob", date);
            fragment.setArguments(bundle);
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, fragment, "FragmentSignupAddressInfo")
                    .commitAllowingStateLoss();
        }


    }


}
