package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.R;
import lit.com.Response.Address;
import lit.com.Response.ProfileViewResponse;
import lit.com.Response.User;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.GetUserToken;
import lit.com.UTILS.SnackBarUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProfile extends Fragment {

    View view;
    String token;
    LITServices services;
    DialogBoxes dialogBoxes;
    TextView name, email, gender, dob, country, state, city, postal_code, phone, address ;
    RadioButton rb_male, rb_female;
    ImageView imageView;
    ImageButton ib_back;
    List<User> userData = new ArrayList<>();
    List<Address> userAddressData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        linkXML();
        return view;
    }

    private void linkXML() {

        name = (TextView) view.findViewById(R.id.profile_tv_name);
        email = (TextView) view.findViewById(R.id.profile_tv_email);
        dob = (TextView) view.findViewById(R.id.profile_tv_dob);
        country = (TextView) view.findViewById(R.id.profile_tv_country);
        state = (TextView) view.findViewById(R.id.profile_tv_state);
        city = (TextView) view.findViewById(R.id.profile_tv_city);
        postal_code = (TextView) view.findViewById(R.id.profile_tv_zip_code);
        phone = (TextView) view.findViewById(R.id.profile_tv_phone);
        address = (TextView) view.findViewById(R.id.profile_tv_address);
        ib_back = (ImageButton) view.findViewById(R.id.ib_back_my_profile);
        rb_male = (RadioButton) view.findViewById(R.id.profile_rb_male);
        rb_female = (RadioButton) view.findViewById(R.id.profile_rb_female);

        imageView = (ImageView) view.findViewById(R.id.profile_iv_gender);

        token = GetUserToken.getUserSession(getActivity());
        if (token!=null) {
            viewProfile(token);
        }

        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }


    private void viewProfile(String token){
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<ProfileViewResponse> executeService = services.getUserProfile(token);
        executeService.enqueue(new Callback<ProfileViewResponse>() {
            @Override
            public void onResponse(Call<ProfileViewResponse> call, Response<ProfileViewResponse> response) {
                assert response.body() != null;
                if (response.body().getStatus().equals(1)){
                   if (response.body().getData().getUser()!=null && response.body().getData().getUser()!=null){
                       userData.add(response.body().getData().getUser());
                       userAddressData.add(response.body().getData().getAddress());
                       initialization();
                   }
                }
                else {
                    DialogBoxes.alterDialog(getActivity(), "Something Went Wrong, Try Again");
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<ProfileViewResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    private void initialization() {

        imageView.setVisibility(View.GONE);
        if (userData.get(0).getGender().equals("Female")){
            imageView.setImageResource(R.drawable.ic_female);
        }
        else {
            imageView.setImageResource(R.drawable.ic_male);
        }

        name.setText(userData.get(0).getFirstName()+" "+userData.get(0).getFirstName()+"");
        email.setText(userData.get(0).getEmail()+"");
        dob.setText(userData.get(0).getDay()+userData.get(0).getMonth()+userData.get(0).getYear()+"");
        country.setText(userAddressData.get(0).getCountry()+"");
        city.setText(userAddressData.get(0).getCity()+"");
        state.setText(userAddressData.get(0).getState()+"");
        phone.setText(userAddressData.get(0).getPhone()+"");
        address.setText(userAddressData.get(0).getAddress()+"");

        if (userData.get(0).getGender().equals("Female")){
            rb_female.setChecked(true);
            rb_male.setChecked(false);
        }
        else {
            rb_female.setChecked(false);
            rb_male.setChecked(true);
        }
    }


}
