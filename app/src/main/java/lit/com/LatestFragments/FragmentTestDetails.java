package lit.com.LatestFragments;

import android.app.Fragment;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.DataBase.Utils.DBTablesUtils;
import lit.com.R;
import lit.com.Response.TestDetailsServicesData;
import lit.com.Response.TestDetailsServicesResponse;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTestDetails extends Fragment {

    View view;
    LITServices services;
    Bundle bundle ;
    DialogBoxes dialogBoxes;
    String test_id = null, user_id;
    UserSession userSession;
//    LabelButtonView test_price;
    TextView test_code, test_type, test_title, test_requirements, test_description, test_sale_price, test_availability, test_price;
    Button add_to_cart;
    ImageButton ib_back_test_details;
    List<TestDetailsServicesData> dataList = new ArrayList<>();
    LITDB litdb;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle!=null){
            test_id = bundle.getString("test_id");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_test_details2, container, false);
        if (test_id!=null) {
            getItemDetails(test_id);
        }
        linkXML();
        return view;
    }

    private void linkXML() {

        test_title = (TextView) view.findViewById(R.id.tv_test_title);
        test_code = (TextView) view.findViewById(R.id.tv_test_code);
//        test_type = (TextView) view.findViewById(R.id.tv_test_type);
        test_requirements = (TextView) view.findViewById(R.id.tv_test_requirements);
        test_description = (TextView) view.findViewById(R.id.tv_test_description);
        test_price = (TextView) view.findViewById(R.id.tv_test_price);
        test_sale_price = (TextView) view.findViewById(R.id.tv_sale_price);
       // test_availability = (TextView) view.findViewById(R.id.tv_test_availability);
        add_to_cart = (Button) view.findViewById(R.id.btn_test_add_cart);
        ib_back_test_details = (ImageButton) view.findViewById(R.id.ib_test_details);
    }

    private void initialization() {

        test_title.setText(dataList.get(0).getName());
        test_code.setText(dataList.get(0).getSku());
     //   test_type.setText(dataList.get(0).getType());

        if (!dataList.get(0).getDescription().equals("")) {
            String converted = String.valueOf(Html.fromHtml(dataList.get(0).getRequirments()));

            test_requirements.setText(converted);
        }
        if (!dataList.get(0).getRequirments().equals("")) {
            String converted = String.valueOf(Html.fromHtml(dataList.get(0).getDescription()));

            test_description.setText(converted);
        }
        test_price.setText("$"+dataList.get(0).getSalePrice());
        test_sale_price.setText("$"+dataList.get(0).getPriceForDoctors());
//        if (dataList.get(0).getInStock().equals(1)) {
//            test_availability.setText("Yes");
//        }

        add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserSession();
                litdb = new LITDB(getActivity());
                boolean ifExist = litdb.getItemExist(LITDB.CartTB,
                        DBTablesUtils.product_id, dataList.get(0).getId().toString());
                if (!ifExist){
                    ContentValues cv = new ContentValues();
                    cv.put(DBTablesUtils.column_id, litdb.getMaxDocNo(LITDB.CartTB));
                    cv.put(DBTablesUtils.product_id, dataList.get(0).getId().toString());
                    cv.put(DBTablesUtils.product_name, dataList.get(0).getName().toString());
                    cv.put(DBTablesUtils.product_price, dataList.get(0).getSalePrice().toString());
                    cv.put(DBTablesUtils.product_quantity, "1");
                    cv.put(DBTablesUtils.user_id, user_id);
                   // cv.put(DBTablesUtils.product_tax, dataListState.get(0).getTax().toString());
                    litdb.insertData(LITDB.CartTB, cv);
                    DialogBoxes.alterDialog(getActivity(),"Added to Cart");

                    int count = litdb.TempList2().size();
                    Constants.counter(getActivity(), count);
                }
                else {
                    DialogBoxes.alterDialog(getActivity(),"Bundle Already Exist in Cart");
                }
            }
        });
        ib_back_test_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void getItemDetails(String id) {

        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<TestDetailsServicesResponse> getAllTests = services.getTestDetails(id);
        getAllTests.enqueue(new Callback<TestDetailsServicesResponse>() {
            @Override
            public void onResponse(Call<TestDetailsServicesResponse> call, Response<TestDetailsServicesResponse> response) {

                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData()!=null){
                        dataList.add(response.body().getData());
                        if (dataList.size()==1){
                            initialization();
                        }
                    }
                }
                else {
                    SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                            "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<TestDetailsServicesResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    private void getUserSession(){

        userSession = new UserSession(getActivity());
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();

        user_id = userSessionDetails.get(UserSession.ID);

        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.TOKEN)));
        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
    }


}
