package lit.com.LatestFragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterNearByLabs;
import lit.com.Activities.MapsActivity;
import lit.com.Response.NearByServicesData;
import lit.com.Response.NearByServicesResponse;
import lit.com.R;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNearByLabs extends Fragment{

    View view;
    ListView listView;
    List<NearByServicesData> dataList = new ArrayList<>();
    AdapterNearByLabs adapter ;
    LITServices services;
    Bundle bundle ;
    ImageButton ib_back;
    DialogBoxes dialogBoxes;
    String city, state, zipcode;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle!=null){
            city = bundle.getString("city");
            state = bundle.getString("state");
            zipcode = bundle.getString("zipcode");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_location2, container, false);
        getNearByLocation(zipcode, state, city);
        return view;
    }

    private void linkXML() {

        listView = (ListView) view.findViewById(R.id.list_searched_labs);
        ib_back = (ImageButton) view.findViewById(R.id.ib_back_searched_locations);

        adapter = new AdapterNearByLabs(getActivity(), dataList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                bundle = new Bundle();
                intent.putExtra("location_id", dataList.get(position).getId().toString());
                startActivity(intent);
            }
        });
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void getNearByLocation(String zip, String state, String city) {

        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<NearByServicesResponse> getAllTests = services.getAllNearByLocations(zip, city, state);
       // Call<NearByServicesResponse> getAllTests = services.getAllNearByLocations(zip, city, state);
        getAllTests.enqueue(new Callback<NearByServicesResponse>() {
            @Override
            public void onResponse(Call<NearByServicesResponse> call, Response<NearByServicesResponse> response) {

                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData()!=null){
                        dataList.addAll(response.body().getData());
                        if (dataList.size()>0){
                            linkXML();
                        }
                    }
                }
                else {
                    SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                            response.body().getMessage(),Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<NearByServicesResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

}
