package lit.com.LatestFragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Activities.DashBoardActivity;
import lit.com.LatestFragments.FragmentSignupPersonalInfo;
import lit.com.R;
import lit.com.Response.LoginServiceResponse;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;

public class FragmentLogin extends Fragment {

    View view;
    Button btn_login;
    String email, password;
    EditText edt_email, edt_password;
    TextView tv_create_account;
    LITServices service;
    DialogBoxes dialogBoxes;
    UserSession userSession;
    String firstName, lastName, userEmail, state, dob, apiToken, gender,
            company, userId, userRoleId, isVerified, token;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
       // return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_login, container, false);
        linkXML();
        return  view;

    }

    private void linkXML() {
        btn_login = (Button) view.findViewById(R.id.btn_login);
        edt_email = (EditText) view.findViewById(R.id.login_edt_email);
        edt_password = (EditText) view.findViewById(R.id.login_edt_pass);
        tv_create_account  = (TextView) view.findViewById(R.id.login_tv_create_account);
        onClickListner();
    }


    private void onClickListner() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUser();
            }
        });
        tv_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment, new FragmentSignupPersonalInfo(),
                                "FragmentSignupPersonalInfo")
                        .addToBackStack("FragmentSignupPersonalInfo")
                        .commitAllowingStateLoss();
            }
        });
    }


    private void validateUser(){
        email = edt_email.getText().toString();
        password = edt_password.getText().toString();

        if (TextUtils.isEmpty(email)){
            edt_email.setError("Enter your email address");
        }
        else if (TextUtils.isEmpty(password)){
            edt_password.setError("Enter your password");
        }
        else {
            executeService();
        }

    }

    private void executeService() {

        userSession = new UserSession(getActivity());
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        service = RetrofitClient.retrofit.create(LITServices.class);
        Call<LoginServiceResponse> executeLogin = service.loginService(email, password);
        executeLogin.enqueue(new Callback<LoginServiceResponse>() {
            @Override
            public void onResponse(Call<LoginServiceResponse> call, retrofit2.Response<LoginServiceResponse> response) {
                if (response.body()!=null){
                    if (response.body().getStatus().equals(1)){
                        if (response.body().getData()!=null){
                            SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content), response.body()
                                    .getMessage(),Snackbar.LENGTH_LONG);

                            firstName = response.body().getData().getFirstName()+"";
                            lastName = response.body().getData().getLastName()+"";
                            apiToken = response.body().getData().getApiToken()+"";
                            token = response.body().getToken()+"";
                            state = response.body().getData().getState()+"";
                            dob = response.body().getData().getDob()+"";
                            company = response.body().getData().getCompany()+"";
                            userEmail = response.body().getData().getEmail()+"";
                            gender = response.body().getData().getGender()+"";
                            userId = response.body().getData().getId().toString()+"";
                            userRoleId = response.body().getData().getRoleId().toString()+"";
                            isVerified = response.body().getData().getIsVerified().toString()+"";

                            userSession.createSession(firstName, lastName, userEmail, userId, userRoleId,
                                    gender, apiToken, token, state, dob, company, isVerified);
                            dialogBoxes.hideProgress();
                            getFragmentManager().popBackStack();
                            DialogBoxes.alterDialog(getActivity(),  response.body()
                                    .getMessage());
                            Constants.loginStatus(getActivity(), "LOGOUT");

                            startActivity(new Intent(getActivity(), DashBoardActivity.class));
                            getActivity().finish();
                        }
                    }
                    else {
                        SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content), response.body()
                                .getMessage(),Snackbar.LENGTH_LONG);
                        dialogBoxes.hideProgress();
                    }
                    dialogBoxes.hideProgress();
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<LoginServiceResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content), "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });


    }

}
