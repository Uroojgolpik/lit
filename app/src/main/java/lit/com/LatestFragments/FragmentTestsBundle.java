package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterBundlesTests;
import lit.com.LatestFragments.FragmentTestsBundlesDetail;
import lit.com.R;
import lit.com.Response.TestBundlesServicesData;
import lit.com.Response.TestBundlesServicesResponse;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentTestsBundle extends Fragment{

    View view;;
    ListView listView;
    List<TestBundlesServicesData> dataList = new ArrayList<>();
    LITServices services;
    Bundle bundle ;
    DialogBoxes dialogBoxes;
    AdapterBundlesTests adapter;
    ImageButton ib_back_bundles;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_bundles, container, false);

        if (dataList.size()<=0) {
            getList();
        }
        else {
            linkXML();
        }
        return view;
    }

    private void linkXML() {
        listView = (ListView) view.findViewById(R.id.list_bundle_test);
        ib_back_bundles = (ImageButton) view.findViewById(R.id.ib_back_bundles);

        initialization();
    }

    private void initialization() {

        adapter = new AdapterBundlesTests(getActivity(), dataList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bundle = new Bundle();
                bundle.putString("id", dataList.get(position).getId().toString());
                bundle.putString("bundle_name", dataList.get(position).getName());
                bundle.putString("bundle_description", dataList.get(position).getDescription());
                Fragment fragment = new FragmentTestsBundlesDetail();
                fragment.setArguments(bundle);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment, fragment, "FragmentTestsBundlesDetail")
                        .addToBackStack("FragmentTestsBundlesDetail")
                        .commitAllowingStateLoss();
            }
        });

        ib_back_bundles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void getList() {
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<TestBundlesServicesResponse> getAllTests = services.getTestBundles();
        getAllTests.enqueue(new Callback<TestBundlesServicesResponse>() {
            @Override
            public void onResponse(Call<TestBundlesServicesResponse> call, Response<TestBundlesServicesResponse> response) {
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataList.clear();
                        dataList.addAll(response.body().getData());
                          if (dataList.size()>0) {
                              linkXML();
                           }

                    }
                    Constants.tax = response.body().getTax().get(0).getPrice();
                }
                else {
                    SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                            "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<TestBundlesServicesResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

}
