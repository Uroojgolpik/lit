package lit.com.LatestFragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterSearchTest;
import lit.com.R;
import lit.com.Response.SearchTestData;
import lit.com.Response.SearchTestResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSearchTest extends Fragment implements AdapterView.OnItemClickListener {

    View view;
    ListView listView;
    List<SearchTestData> dataList = new ArrayList<>();
    AdapterSearchTest adapter ;
    LITServices services;
    DialogBoxes dialogBoxes;
    AlertDialog alertDialog1;
    UserSession session;
    String keyword;
    ImageButton ib_back_test;
    String TAG = "FragmentTest";
    Bundle bundle;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle!=null){
            keyword =  bundle.getString("keyword");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_test_search_list, container, false);
        linkXML();
        if (dataList.size()<=0) {
            getList(keyword);
        }
        return view;
    }


    private void linkXML() {
        listView = (ListView) view.findViewById(R.id.search_list_test);
        listView.setOnItemClickListener(this);
        ib_back_test = (ImageButton) view.findViewById(R.id.ib_back_search_test);
        ib_back_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        initialization();
    }

    private void initialization() {

        adapter = new AdapterSearchTest(getActivity(), dataList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getList(String keyword) {
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        final Call<SearchTestResponse> getAllTests = services.searchTests(keyword);
        getAllTests.enqueue(new Callback<SearchTestResponse>() {
            @Override
            public void onResponse(Call<SearchTestResponse> call, Response<SearchTestResponse> response) {
                assert response.body() != null;
                if (response.body().getStatus().equals(1)) {
                    if (response.body().getData() != null) {
                        if (response.body().getData().size() > 0) {
                            dataList.clear();
                            dataList.addAll(response.body().getData());
                            initialization();

                        }
                    }

                }
                else {
                    DialogBoxes.alterDialog(getActivity(), response.body().getMessage());
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<SearchTestResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        bundle = new Bundle();
        bundle.putString("test_id", dataList.get(position).getId().toString());

        Fragment fragment = new FragmentTestDetails();
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, fragment, "FragmentTestDetails")
                .addToBackStack("FragmentTestDetails")
                .commitAllowingStateLoss();
    }
}
