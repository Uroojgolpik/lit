package lit.com.LatestFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lit.com.Activities.CheckoutActivity;
import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.DataBase.Models.CartModel;
import lit.com.DataBase.Utils.DBTablesUtils;
import lit.com.R;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.UserSession;

public class FragmentCart extends Fragment {

    View view;
    ListView listview_cart;
    AdapterCart adapterCart;
    UserSession userSession;
    LITDB litdb;
    int total, count;
    String TAG = "FragmentCart";
    TextView tv_order_total, tv_tax_included;
    public String user_id;
    List<CartModel> dataList = new ArrayList<>();
    Button btn_checkout;
    ImageButton ib_back_cart;
    TextView toolbar_cart_counter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_cart, container, false);
        getUserSession();
        populateData();
        linkXML(view);
        return view;
    }


    private void linkXML(View view) {

        tv_order_total = (TextView) view.findViewById(R.id.tv_order_total);
        tv_tax_included = (TextView) view.findViewById(R.id.tv_tax_included);
        listview_cart = (ListView) view.findViewById(R.id.listview_cart);
        btn_checkout = (Button) view.findViewById(R.id.btn_checkout);
        ib_back_cart = (ImageButton) view.findViewById(R.id.ib_back_cart);
        initialization();
    }

    private void initialization() {
        litdb = new LITDB(getActivity());
        adapterCart = new AdapterCart(getActivity(), dataList);
        listview_cart.setAdapter(adapterCart);
        adapterCart.notifyDataSetChanged();

        if (litdb.getTotalPrice()!=null) {
            total = Integer.parseInt(litdb.getTotalPrice()) + Constants.tax;
        }

        Log.d(TAG, "initialization: "+ total);
         tv_tax_included.setText("$ "+ Constants.tax);
        tv_order_total.setText("S "+total);

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userSession.checkLogin()) {
                    startActivity(new Intent(getActivity(), CheckoutActivity.class));
                }
                else {
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.fragment, new FragmentLogin(), "FragmentLogin")
                            .addToBackStack("FragmentLogin")
                            .commitAllowingStateLoss();
                }
            }
        });
        ib_back_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void populateData() {
            litdb = new LITDB(getActivity());
            dataList = litdb.TempList2();
            count = dataList.size();
    }


    private void getUserSession(){
        userSession = new UserSession(getActivity());
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();

        user_id = userSessionDetails.get(UserSession.ID);

        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.ID)));
        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
    }

    public class AdapterCart extends BaseAdapter {

        LITDB litdb;
        Activity context;
        List<CartModel> dataList;


        public AdapterCart(Activity context, List<CartModel> dataList) {

            this.context = context;
            this.dataList = dataList;
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public Object getItem(int position) {
            return dataList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.custom_cart_item, parent, false);


            final TextView product_name = (TextView) view.findViewById(R.id.tv_product_name);
            TextView product_price = (TextView) view.findViewById(R.id.tv_product_price);
            LinearLayout iv_delete_product = (LinearLayout) view.findViewById(R.id.ll_remove);

            product_name.setText("Product: "+dataList.get(position).getProduct_Name());
            product_price.setText("Price: $"+dataList.get(position).getProduct_Price());

            iv_delete_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    litdb = new LITDB(context);
                    litdb.deleteMethod(dataList.get(position).getProduct_ID(), LITDB.CartTB, DBTablesUtils.product_id);
                    dataList.clear();
                    dataList = litdb.TempList2();
                    listview_cart.setAdapter(adapterCart);
                    notifyDataSetChanged();
                    int count = litdb.TempList2().size();
                    Constants.counter(getActivity(), count);


                }
            });
            return view;
        }


    }


}

