package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import lit.com.R;

public class FragmentTestsBundlesDetail extends Fragment {

    View view;
    Bundle bundle ;
    ImageButton ib_back,ib_close_btn;
    TextView tv_bundle_description, tv_bundle_heading;
    String id, description, name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        if (bundle!=null){
            id = bundle.getString("id");
            description = bundle.getString("bundle_description");
            name = bundle.getString("bundle_name");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bundle_details, container, false);
        linkXML();

        return view;
    }


    private void linkXML() {

        ib_back = (ImageButton) view.findViewById(R.id.ib_back_bundles_details);
        ib_close_btn = (ImageButton) view.findViewById(R.id.ib_close_btn);
        tv_bundle_description = (TextView) view.findViewById(R.id.tv_bundle_description);
        tv_bundle_heading = (TextView) view.findViewById(R.id.tv_bundle_heading);

        initialization();

    }

    private void initialization() {


        tv_bundle_heading.setText(name);

        if (!description.equals("")) {
            String converted = String.valueOf(Html.fromHtml(description));

            tv_bundle_description.setText(converted);
        }

        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        ib_close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

    }





}
