package lit.com.LatestFragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterTest;
import lit.com.R;
import lit.com.Response.TestServiceData;
import lit.com.Response.TestServiceResponse;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTest extends Fragment implements AdapterView.OnItemClickListener {

    View view;
    ListView listView;
    List<TestServiceData> dataList = new ArrayList<>();
    AdapterTest adapter ;
    LITServices services;
    Bundle bundle ;
    DialogBoxes dialogBoxes;
    AlertDialog alertDialog1;
    UserSession session;
    ImageButton ib_back_test;
    String TAG = "FragmentTest";
    EditText ed_seatch_tests;
    ArrayList<String> alphabeticOrders = new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_test_list, container, false);
        linkXML();
        if (dataList.size()<=0) {
            getList();
        }
        return view;
    }


    private void linkXML() {
        listView = (ListView) view.findViewById(R.id.list_test);
        listView.setOnItemClickListener(this);
        ib_back_test = (ImageButton) view.findViewById(R.id.ib_back_test);
        ed_seatch_tests = (EditText) view.findViewById(R.id.ed_search_tests);


        initialization();
    }

    private void initialization() {

        adapter = new AdapterTest(getActivity(), dataList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        ib_back_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        ed_seatch_tests.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String keyword = ed_seatch_tests.getText().toString();
                    performSearch(keyword);
                    return true;
                }
                return false;
            }
        });
    }

    private void getList() {
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        final Call<TestServiceResponse> getAllTests = services.getAllTests();
        getAllTests.enqueue(new Callback<TestServiceResponse>() {
            @Override
            public void onResponse(Call<TestServiceResponse> call, Response<TestServiceResponse> response) {
                assert response.body() != null;
                if (response.body().getStatus().equals(1)) {
                    if (response.body().getData() != null) {
                        if (response.body().getData().size() > 0) {
                            dataList.clear();
                            dataList.addAll(response.body().getData());
                            for (int i = 0; i < dataList.size(); i++){
                                String  string = dataList.get(i).getName();
                                if (!string.equals(null) && !string.equals("")) {
                                    String s = string.substring(0, 1);
                                    if (alphabeticOrders.contains(s)){
                                        dataList.get(i).setAlphabet("");

                                    }
                                    else {
                                        dataList.get(i).setAlphabet(s);
                                        alphabeticOrders.add(s);
                                        Log.d(TAG, "getAlphabeticList: "+ s+" item not exist.");
                                    }
                                }


                            }
                            Constants.tax = response.body().getTax().get(0).getPrice();
                            Log.d(TAG, "getAlphabeticList: "+ alphabeticOrders);
                            System.out.print(alphabeticOrders);

                            initialization();

                        }
                    }

                }
                else {
                    DialogBoxes.alterDialog(getActivity(), response.body().getMessage());
//                    SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
//                            "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<TestServiceResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        bundle = new Bundle();
        bundle.putString("test_id", dataList.get(position).getId().toString());

        Fragment fragment = new FragmentTestDetails();
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, fragment, "FragmentTestDetails")
                .addToBackStack("FragmentTestDetails")
                .commitAllowingStateLoss();
    }
    private void performSearch(String keyword) {

        Bundle bundle = new Bundle();
        Fragment fragment = new FragmentSearchTest();
        bundle.putString("keyword", keyword);
        fragment.setArguments(bundle);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment,fragment, "FragmentSearchTest")
                .addToBackStack("FragmentSearchTest")
                .commitAllowingStateLoss();

    }

}
