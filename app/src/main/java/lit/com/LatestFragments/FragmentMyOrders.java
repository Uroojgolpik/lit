package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.AdapterMyOrders;
import lit.com.R;
import lit.com.Response.MyOrdersServicesData;
import lit.com.Response.MyOrdersServicesResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.GetUserToken;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMyOrders extends Fragment implements AdapterView.OnItemClickListener {

    View view;
    ListView listView;
    List<MyOrdersServicesData> dataList = new ArrayList<>();
    AdapterMyOrders adapter ;
    LITServices services;
    Bundle bundle ;
    DialogBoxes dialogBoxes;
    UserSession session;
    ImageButton ib_back;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_orders, container, false);
        linkXML();
        if (dataList.size()<=0) {
            GetUserToken getUserToken = new GetUserToken();
            String token = getUserToken.getUserSession(getActivity());
            getList(token);
        }
        return view;
    }

    private void linkXML() {
        listView = (ListView) view.findViewById(R.id.my_orders_list);
        ib_back = (ImageButton) view.findViewById(R.id.ib_back_my_orders);

        listView.setOnItemClickListener(this);
        initialization();
    }

    private void initialization() {

        adapter = new AdapterMyOrders(getActivity(), dataList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void getList(String token) {
        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<MyOrdersServicesResponse> getMyOders = services.getMyOrders(token);
        getMyOders.enqueue(new Callback<MyOrdersServicesResponse>() {
            @Override
            public void onResponse(Call<MyOrdersServicesResponse> call, Response<MyOrdersServicesResponse> response) {
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataList.clear();
                        dataList.addAll(response.body().getData());
                        initialization();
                    }
                }
                else {
                    SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                            "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<MyOrdersServicesResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        bundle = new Bundle();
//        bundle.putString("test_id", dataListState.get(position).getId().toString());
//
//        Fragment fragment = new FragmentTestDetails();
//        fragment.setArguments(bundle);
//        getFragmentManager()
//                .beginTransaction()
//                .replace(R.id.fragment, fragment, "FragmentTestDetails")
//                .addToBackStack("FragmentTestDetails")
//                .commitAllowingStateLoss();
    }
}
