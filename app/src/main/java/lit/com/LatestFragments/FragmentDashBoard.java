package lit.com.LatestFragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import lit.com.R;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.UserSession;

public class FragmentDashBoard extends Fragment {


    View view;
    LinearLayout cv_test, cv_bundle_test, cv_nearby_location, cv_track_test, cv_my_orders, cv_seacrh_test;
    UserSession userSession;
    Button sigunup_btn;
    EditText ed_search_test;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_dashboard3, container, false);
        linkXML();
        onClickListner();
        return view;
    }



    private void linkXML() {

        cv_test = (LinearLayout) view.findViewById(R.id.cv_test);
        cv_bundle_test = (LinearLayout) view.findViewById(R.id.cv_test_bundle);
        cv_nearby_location = (LinearLayout) view.findViewById(R.id.cv_nearby_location);
        ed_search_test = (EditText) view.findViewById(R.id.ed_search_test);
        sigunup_btn = (Button ) view.findViewById(R.id.dashboard_sigunup_btn);

        userSession = new UserSession(getActivity());
        if (!userSession.checkLogin()) {
            Constants.loginStatus(getActivity(), "LOGOUT");
            sigunup_btn.setVisibility(View.GONE);
        }
        else {
            Constants.loginStatus(getActivity(), "LOGIN");
            sigunup_btn.setVisibility(View.VISIBLE);
        }
    }


    private void onClickListner() {

        cv_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new FragmentTest(), "FragmentNearByLabs");
            }
        });
        cv_bundle_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new FragmentTestsBundle(), "FragmentTestsBundle");
            }
        });
        cv_nearby_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new FragmentNearBySearchLabs(), "FragmentNearBySearchLabs");
            }
        });
        sigunup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserSession userSession = new UserSession(getActivity());
                if (userSession.checkLogin()){
                    replaceFragment(new FragmentSignupPersonalInfo(), "FragmentSignupPersonalInfo");
                }
                else {
                    DialogBoxes.alterDialog(getActivity(), "Please Logout First!");
                }

            }
        });
        ed_search_test.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String keyword = ed_search_test.getText().toString();
                    performSearch(keyword);
                    return true;
                }
                return false;
            }
        });
    }

    private void performSearch(String keyword) {

        Bundle bundle = new Bundle();
        Fragment fragment = new FragmentSearchTest();
        bundle.putString("keyword", keyword);
        fragment.setArguments(bundle);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment,fragment, "FragmentSearchTest")
                .addToBackStack("FragmentSearchTest")
                .commitAllowingStateLoss();

    }

    private void replaceFragment(Fragment fragment, String tag) {

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();

    }
}
