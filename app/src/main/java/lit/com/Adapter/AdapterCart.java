package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.DataBase.Models.CartModel;
import lit.com.DataBase.Utils.DBTablesUtils;
import lit.com.R;

public class AdapterCart extends BaseAdapter{

    LITDB litdb;
    Activity context;
    List<CartModel> dataList;


    public AdapterCart(Activity context, List<CartModel> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_cart_item, parent, false);


        final TextView product_name = (TextView) view.findViewById(R.id.tv_product_name);
        TextView product_price = (TextView) view.findViewById(R.id.tv_product_price);
        LinearLayout iv_delete_product = (LinearLayout) view.findViewById(R.id.ll_remove);

        product_name.setText("Product: "+dataList.get(position).getProduct_Name());
        product_price.setText("Price: $"+dataList.get(position).getProduct_Price());

        iv_delete_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                litdb = new LITDB(context);
                litdb.deleteMethod(dataList.get(position).getProduct_ID(), LITDB.CartTB, DBTablesUtils.product_id);
               // FragmentCart.adapterCart.notifyDataSetChanged();
//                FragmentCart.populateData(context);
            }
        });
        return view;
    }


}
