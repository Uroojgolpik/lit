package lit.com.Adapter;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

import lit.com.Response.StateServicesData;

public class SpinnerAdapter extends ArrayAdapter<StateServicesData> {

    private Activity context;
    List<StateServicesData> data;

     public SpinnerAdapter(Activity context, int textViewResourceId, List<StateServicesData> data) {

         super(context, textViewResourceId, data);
         this.context = context;
         this.data = data;
     }

     public int getCount(){
        return data.size();
     }


     public long getItemId(int position){
        return position;
     }

     @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH)
     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
         TextView label = new TextView(context);

         label.setText(data.get(position).getTitle());
         label.setPadding(5,5,5,5);
         label.setAllCaps(true);
         label.setTextSize(16);
         return label;
     }

     @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH)
     @Override
     public View getDropDownView(int position, View convertView,
                                 ViewGroup parent) {
         TextView label = new TextView(context);
         label.setText(data.get(position).getTitle());
         label.setPadding(5,5,5,5);
         label.setAllCaps(true);
         label.setTextSize(16);
         return label;
     }
 }


