package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lit.com.R;
import lit.com.Response.SearchTestData;
import lit.com.Response.TestServiceData;

public class AdapterSearchTest extends BaseAdapter{

    Activity context;
    List<SearchTestData> dataList;
    String TAG = "AdapterTest";
    Boolean isSelected = false;
    ArrayList<String> alphabeticOrders = new ArrayList<>();

    public AdapterSearchTest(Activity context, List<SearchTestData> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_test_items2, parent, false);


        LinearLayout ll_alphabetic_order = (LinearLayout) view.findViewById(R.id.ll_alphabetic_order);
        TextView tv_test_letter = (TextView) view.findViewById(R.id.tv_test_letter);
        TextView tv_test_title = (TextView) view.findViewById(R.id.tv_test_title);
        TextView tv_test_code = (TextView) view.findViewById(R.id.tv_test_code);

        ll_alphabetic_order.setVisibility(View.GONE);
        tv_test_title.setText(dataList.get(position).getName());
        tv_test_code.setText(dataList.get(position).getSku());


        return view;
    }


}
