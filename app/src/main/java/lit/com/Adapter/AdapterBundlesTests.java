package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.DataBase.Utils.DBTablesUtils;
import lit.com.R;
import lit.com.Response.TestBundlesServicesData;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.UserSession;

public class AdapterBundlesTests extends BaseAdapter {

    LayoutInflater layoutInflater;
    Activity context;
    LITDB litdb;
    List<TestBundlesServicesData> dataList = new ArrayList<>();
    String test_id = null, user_id;
    UserSession userSession;

    public AdapterBundlesTests(Activity context, List<TestBundlesServicesData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"ViewHolder", "SetTextI18n"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
       View view = layoutInflater.inflate(R.layout.custom_bundles_items, parent, false);


        TextView tv_title = (TextView) view.findViewById(R.id.bundle_test_name);
        TextView tv_price = (TextView) view.findViewById(R.id.bundle_test_price);
        LinearLayout btn_cart = (LinearLayout) view.findViewById(R.id.bundle_add_to_cart);

        tv_title.setText(dataList.get(position).getName());
        tv_price.setText("$ "+ dataList.get(position).getPrice());

        btn_cart.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                getUserSession();
                litdb = new LITDB(context);
                boolean ifExist = litdb.getItemExist(LITDB.CartTB,
                        DBTablesUtils.product_id,
                        dataList.get(position).getId().toString());
                if (!ifExist){
                    ContentValues cv = new ContentValues();
                    cv.put(DBTablesUtils.column_id, litdb.getMaxDocNo(LITDB.CartTB));
                    cv.put(DBTablesUtils.product_id, dataList.get(position).getId().toString());
                    cv.put(DBTablesUtils.product_name, dataList.get(position).getName().toString());
                    cv.put(DBTablesUtils.product_price, dataList.get(position).getPrice().toString());
                    cv.put(DBTablesUtils.product_quantity, "1");
                    cv.put(DBTablesUtils.user_id, user_id);

                    litdb.insertData(LITDB.CartTB, cv);
                    DialogBoxes.alterDialog(context,"Added to Cart");

                    int count = litdb.TempList2().size();
                    Constants.counter(context, count);

                }
                else {
                    DialogBoxes.alterDialog(context,"Bundle Already Exist in Cart");
                }
                //this will log the page number that was click
//                Log.i("TAG", "This page was clicked: " + position);

            }
        });


        return view;
    }
    private void getUserSession(){

        userSession = new UserSession(context);
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();

        user_id = userSessionDetails.get(UserSession.ID);

        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.TOKEN)));
        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
    }

}
