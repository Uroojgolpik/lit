package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import lit.com.R;
import lit.com.Response.MyOrdersServicesData;
import lit.com.Response.TestServiceData;

public class AdapterMyOrders extends BaseAdapter{


    Context context;
    List<MyOrdersServicesData> dataList;


    public AdapterMyOrders(Context context, List<MyOrdersServicesData> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_myorders_items, parent, false);


        TextView tv_my_order_id = (TextView) view.findViewById(R.id.tv_my_order_id);
        TextView tv_my_order_status = (TextView) view.findViewById(R.id.tv_my_order_status);
        TextView tv_my_order_payment_type = (TextView) view.findViewById(R.id.tv_my_order_payment_type);
        TextView tv_grand_total = (TextView) view.findViewById(R.id.tv_grand_total);
        TextView tv_my_order_date = (TextView) view.findViewById(R.id.tv_my_order_date);

        tv_my_order_id.setText("Order ID: "+dataList.get(position).getId());
        tv_my_order_status.setText("Order Status: "+dataList.get(position).getOrderStatus());
        tv_my_order_payment_type.setText("Payment Type: "+dataList.get(position).getPaymentType());
        tv_grand_total.setText("$ "+dataList.get(position).getGrandTotal());
        tv_my_order_date.setText( dataList.get(position).getCreatedAt());

        return view;
    }


}
