package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lit.com.R;
import lit.com.Response.TestServiceData;

public class AdapterTest extends BaseAdapter{

    Activity context;
    List<TestServiceData> dataList;
    String TAG = "AdapterTest";
    Boolean isSelected = false;
    ArrayList<String> alphabeticOrders = new ArrayList<>();

    public AdapterTest(Activity context, List<TestServiceData> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_test_items2, parent, false);


        LinearLayout ll_alphabetic_order = (LinearLayout) view.findViewById(R.id.ll_alphabetic_order);
        TextView tv_test_letter = (TextView) view.findViewById(R.id.tv_test_letter);
        TextView tv_test_title = (TextView) view.findViewById(R.id.tv_test_title);
      //  TextView tv_test_price = (TextView) view.findViewById(R.id.tv_test_price);
        TextView tv_test_code = (TextView) view.findViewById(R.id.tv_test_code);
  //      TextView btn_view_details = (TextView) view.findViewById(R.id.btn_view_details);

//        for (int i = 0; i < dataList.size(); i++){
//            String  string = dataList.get(i).getName();
//            if (!string.equals(null) && !string.equals("")) {
//                String s = string.substring(0, 1);
//                if (alphabeticOrders.contains(s)){
//                    isSelected = false;
//                    ll_alphabetic_order.setVisibility(View.GONE);
//                }
//
//                else {
//                    alphabeticOrders.add(s);
//                    Log.d(TAG, "getAlphabeticList: "+ s+" item not exist.");
//                    ll_alphabetic_order.setVisibility(View.VISIBLE);
//                    tv_test_letter.setText(s);
//
//                }
//            }
//
//
//        }
//        System.out.print(alphabeticOrders);
//        Log.d(TAG, "getAlphabeticList: "+ alphabeticOrders);

        if (!dataList.get(position).getAlphabet().equals("")){
            ll_alphabetic_order.setVisibility(View.VISIBLE);
            tv_test_letter.setText(dataList.get(position).getAlphabet());
        }
        else {
            ll_alphabetic_order.setVisibility(View.GONE);
        }
        tv_test_title.setText(dataList.get(position).getName());
//        tv_test_price.setText("Test Price: "+dataList.get(position).getSalePrice());
        tv_test_code.setText(dataList.get(position).getSku());

//        btn_view_details.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putString("test_id", dataList.get(position).getId().toString());
//
//                Fragment fragment = new FragmentTestDetails();
//                fragment.setArguments(bundle);
//                context.getFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment, fragment, "FragmentTestDetails")
//                        .addToBackStack("FragmentTestDetails")
//                        .commitAllowingStateLoss();
//            }
//        });
        return view;
    }

    private void getAlphabeticList() {

        alphabeticOrders.clear();
        for (int i = 0; i < dataList.size(); i++){
            String  string = dataList.get(i).getName();
            if (!string.equals(null) && !string.equals("")) {
                String s = string.substring(0, 1);
                if (!alphabeticOrders.contains(s)){
                    alphabeticOrders.add(s);
                    Log.d(TAG, "getAlphabeticList: "+ s+" item already exist.");
                }
            }
        }

        Log.d(TAG, "getAlphabeticList: "+ alphabeticOrders);
        System.out.print(alphabeticOrders);

    }
}
