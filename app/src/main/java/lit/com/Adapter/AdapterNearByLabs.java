package lit.com.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import lit.com.Response.NearByServicesData;
import lit.com.R;

public class AdapterNearByLabs extends BaseAdapter{


    Context context;
    List<NearByServicesData> dataList;


    public AdapterNearByLabs(Context context, List<NearByServicesData> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_location_items, parent, false);


        TextView tv_test_title = (TextView) view.findViewById(R.id.tv_location_title);
        TextView tv_location_address = (TextView) view.findViewById(R.id.tv_location_address);

        tv_test_title.setText(dataList.get(position).getName());
        tv_location_address.setText(dataList.get(position).getAddress());

        return view;
    }


}
