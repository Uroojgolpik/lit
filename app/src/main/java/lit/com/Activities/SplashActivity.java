package lit.com.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import lit.com.R;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);
        startIntent();
    }

    private void startIntent() {

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                finish();
            }
        },2500);

    }

}
