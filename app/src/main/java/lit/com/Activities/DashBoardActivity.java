package lit.com.Activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.LatestFragments.FragmentCart;
import lit.com.LatestFragments.FragmentDashBoard;
import lit.com.LatestFragments.FragmentLogin;
import lit.com.LatestFragments.FragmentMyOrders;
import lit.com.LatestFragments.FragmentProfile;
import lit.com.R;
import lit.com.Response.LogoutResponse;
import lit.com.UTILS.Constants;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import lit.com.UTILS.Validations;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    LITServices services;
    DialogBoxes dialogBoxes;
    UserSession userSession;
    DrawerLayout drawer;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    TextView nav_user_name, nav_user_email, toolbar_cart_counter, tv_login_logout;
    LinearLayout toolbar_login_button;
    String email, name, token ;
    LITDB litdb;
    RelativeLayout toolbar_cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        litdb = new LITDB(this);
        Validations.multiplePermissions(this);
        linkXLM();
        attachHomeFragment();
}

    private void attachHomeFragment() {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, new FragmentDashBoard(), "\"FragmentDashBoard\"")
                .commitAllowingStateLoss();
    }

    private void linkXLM() {
        getUserSession();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        toolbar_login_button = (LinearLayout) findViewById(R.id.toolbar_login_button);
        toolbar_cart_counter = (TextView) findViewById(R.id.toolbar_cart_counter);
        tv_login_logout  = (TextView) findViewById(R.id.tv_login_logout);
        toolbar_cart =  (RelativeLayout) findViewById(R.id.toolbar_cart);

        initialization();

    }

    private void initialization() {
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationSession(navigationView);
        toolbar_login_button.setOnClickListener(this);
        toolbar_cart.setOnClickListener(this);

        int count = litdb.TempList2().size();
        Constants.counter(this, count);


        if (!userSession.checkLogin()) {
            Constants.loginStatus(this, "LOGOUT");
        }
        else {
            Constants.loginStatus(this, "LOGIN");
        }
    }

    private void navigationSession(NavigationView navigationView) {
        View v = navigationView.getHeaderView(0);

        nav_user_name = (TextView) v.findViewById(R.id.nav_user_name);
        nav_user_email = (TextView) v.findViewById(R.id.nav_user_email);

        nav_user_name.setText(name);
        nav_user_email.setText(email);
        if (userSession.checkLogin()) {
            navigationView.getMenu().findItem(R.id.nav_profile).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_my_orders).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
    //        navigationView.getMenu().removeItem(R.id.nav_logout);
        }
        else {

            navigationView.getMenu().findItem(R.id.nav_profile).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_my_orders).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            getBackToHome();
        } else if (id == R.id.nav_profile) {
            if (!userSession.checkLogin()) {
                replaceFragment(new FragmentProfile(), "FragmentProfile");
            }
            else {
                DialogBoxes.alterDialog(DashBoardActivity.this, "No Session Found!");
            }

        }

        else if (id == R.id.nav_my_orders) {
            if (!userSession.checkLogin()) {
                replaceFragment(new FragmentMyOrders(), "FragmentMyOrders");
            }
            else {
                DialogBoxes.alterDialog(DashBoardActivity.this, "No Session Found!");
            }

        } else if (id == R.id.nav_logout) {
            if (!userSession.checkLogin()) {
                logout();
            }
            else {
                DialogBoxes.alterDialog(DashBoardActivity.this, "No Session Found!");
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment, String tag) {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    private void getUserSession(){
        userSession = new UserSession(this);
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();

        email = userSessionDetails.get(UserSession.EMAIL);
        name = userSessionDetails.get(UserSession.USERNAME);
        token = userSessionDetails.get(UserSession.APITOKEN);

        Log.d("EMAIL", ""+ String.valueOf(userSessionDetails.get(UserSession.EMAIL)));
        Log.d("USERNAME", ""+ String.valueOf(userSessionDetails.get(UserSession.USERNAME)));
        Log.d("APITOKEN", ""+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
        Log.d("getUserSession", "getUserSession: "+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
    }

    private void getBackToHome(){
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        if (count>=0) {
            for (int i = 0; i < count; ++i) {
                fm.popBackStackImmediate();
            }
        }
        attachHomeFragment();
    }

    private void logout(){
        userSession = new UserSession(this);
        dialogBoxes = new DialogBoxes(this);
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<LogoutResponse> executeService = services.logout(token);
        executeService.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                assert response.body() != null;
              //  Intent intent = new Intent(DashBoardActivity.this, DashBoardActivity.class);
                if (response.body().getStatus().equals(1)){
                    userSession.logoutUser();
                    getBackToHome();
                    litdb.deleteList(LITDB.CartTB);
                    int count = litdb.TempList2().size();
                    Constants.counter(DashBoardActivity.this, count);
                    DialogBoxes.alterDialog(DashBoardActivity.this, response.body().getMessage());
                    startActivity(new Intent(DashBoardActivity.this, DashBoardActivity.class));
                    finish();
                }
                else {
                    if (response.body().getMessage().equals("Please Login first") || response.body().getMessage().equals("Unauthorized token")){
                        userSession.logoutUser();
                        DialogBoxes.alterDialog(DashBoardActivity.this, "Your Session Expired");
                        litdb.deleteList(LITDB.CartTB);
                        Constants.loginStatus(DashBoardActivity.this, "LOGIN");
//                        getBackToHome();
                    }
                    else {
                        userSession.logoutUser();
                        DialogBoxes.alterDialog(DashBoardActivity.this, "Your Session Expired");
                        litdb.deleteList(LITDB.CartTB);
                        Constants.loginStatus(DashBoardActivity.this, "LOGIN");
                        getBackToHome();
                    }

                    int count = litdb.TempList2().size();
                    Constants.counter(DashBoardActivity.this, count);
                    startActivity(new Intent(DashBoardActivity.this, DashBoardActivity.class));
                    finish();
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.toolbar_login_button:
                if (tv_login_logout.getText().equals("LOGIN")){
                    replaceFragment(new FragmentLogin(), "FragmentLogin");

//                    if (userSession.checkLogin()) {
//                        replaceFragment(new FragmentLogin(), "FragmentLogin");
//                    } else {
//                        DialogBoxes.alterDialog(this, "Please Logout First!");
//                    }
                }
                if (tv_login_logout.getText().equals("LOGOUT")){
                    logout();
                }


                break;
            case R.id.toolbar_cart:
                int count = litdb.TempList2().size();
                if (count>0) {
                    replaceFragment(new FragmentCart(), "FragmentCart");
                }
                else {
                    DialogBoxes.alterDialog(this, "Cart is empty");
                }
                break;
        }
    }
}
