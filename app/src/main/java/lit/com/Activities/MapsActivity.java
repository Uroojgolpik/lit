package lit.com.Activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.R;
import lit.com.Response.NearByDetailServicesData;
import lit.com.Response.NearByDetailServicesResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Bundle bundle = new Bundle();
    Intent i;
    String location_id, address;
    TextView tv_map_title, tv_map_address, tv_map_operation_hours, tv_map_phone, tv_map_fax;
    SupportMapFragment mapFragment;
    LITServices services;
    DialogBoxes dialogBoxes;
    ImageButton ib_location_details;
    String test_id = null;
    Double latitude, longitude;
    List<NearByDetailServicesData> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        i = new Intent();
        i = getIntent();

        if (i!=null){
             location_id = i.getStringExtra("location_id");
            if (!location_id.equals("")){
                getNearByLocationDetails(location_id);
            }
        }
    //    linkXML();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

    private void linkXML() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        ib_location_details = (ImageButton) findViewById(R.id.ib_location_details);
        tv_map_title = (TextView) findViewById(R.id.tv_map_title);
        tv_map_address = (TextView) findViewById(R.id.tv_map_address);
        tv_map_operation_hours = (TextView) findViewById(R.id.tv_map_operation_hours);
        tv_map_phone = (TextView) findViewById(R.id.tv_map_phone);
        tv_map_fax = (TextView) findViewById(R.id.tv_map_fax);

        mapFragment.getMapAsync(this);
        initialization();
    }

    private void initialization() {

        tv_map_address.setText(dataList.get(0).getName());
        tv_map_title.setText(dataList.get(0).getAddress());
        tv_map_operation_hours.setText(dataList.get(0).getOperationHours());
        tv_map_phone.setText(dataList.get(0).getPhone());
        tv_map_fax.setText(dataList.get(0).getFax());
        ib_location_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        if (dataList.get(0).getLatitude()!=null && dataList.get(0).getLongitude()!=null){
            latitude = dataList.get(0).getLatitude();
            longitude = dataList.get(0).getLongitude();
            LatLng location = new LatLng(latitude, longitude);
            mMap.addMarker(new MarkerOptions().position(location)
                    .title(dataList.get(0).getName())
                    .snippet(dataList.get(0).getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 12f));

        }


    }
    private void getNearByLocationDetails(String location_id) {

        dialogBoxes = new DialogBoxes(this);
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<NearByDetailServicesResponse> getLocationDetails = services.getNearByLocationDetails(location_id);
        getLocationDetails.enqueue(new Callback<NearByDetailServicesResponse>() {
            @Override
            public void onResponse(Call<NearByDetailServicesResponse> call, Response<NearByDetailServicesResponse> response) {

                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData()!=null){
                        dataList.add(response.body().getData());
                        if (dataList.size()==1){
                            linkXML();
                        }
                    }
                }
                else {
                    SnackBarUtils.showSnackBar(findViewById(android.R.id.content),
                            "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                }
                dialogBoxes.hideProgress();
            }

            @Override
            public void onFailure(Call<NearByDetailServicesResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();
            }
        });
    }
}
