package lit.com.Activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import lit.com.Fragments.FragmentCheckoutPersonalInfo;
import lit.com.R;

public class CheckoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        linkXML();

    }

    private void linkXML() {
        initialization();
    }
    private void initialization() {
        replaceFragment(new FragmentCheckoutPersonalInfo(), "FragmentCheckoutPersonalInfo");
    }
    private void replaceFragment(Fragment fragment, String tag) {

        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_checkout, fragment, tag)
                .commitAllowingStateLoss();

    }



}
