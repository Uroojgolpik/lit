package lit.com.DataBase.DBExcutionFile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import lit.com.DataBase.Models.CartModel;
import lit.com.DataBase.Utils.DBTablesUtils;

public class LITDB extends SQLiteOpenHelper {

    public static  String DatabaseName = "LITDB";
    public static  Integer DatabaseVersion = 1;
    public static  String CartTB = "Table_Cart";
    public static  String PersonalTB = "Table_Personal";
    public static  String CheckoutTB = "Table_Checkout";
    SQLiteDatabase db;

    public LITDB(@Nullable Context context) {
        super(context, DatabaseName, null, DatabaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBTablesUtils.CREATE_TABLE_CT);
        db.execSQL(DBTablesUtils.CREATE_TABLE_PT);
        db.execSQL(DBTablesUtils.CREATE_TABLE_COT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF  EXISTS " + DBTablesUtils.CREATE_TABLE_CT );
        db.execSQL("DROP TABLE IF  EXISTS " + DBTablesUtils.CREATE_TABLE_PT );
        db.execSQL("DROP TABLE IF  EXISTS " + DBTablesUtils.CREATE_TABLE_COT );

        onCreate(db);
    }

    public void insertData(String TABLE_NAME, ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();

        long rowInserted = db.insert(TABLE_NAME, null, contentValues);
        if (rowInserted != -1) {
            Log.v("Inserting..", contentValues + "");
        }

        else{
            Log.v("Failed to Insert...", contentValues + "");
        }
        db.close();
    }

    @Nullable
    public Cursor rawQuery(String query, String[] where) {
        SQLiteDatabase db;
        db = this.getReadableDatabase();
        try {
            return db.rawQuery(query, where);
        } catch (Exception e) {
            return null;
        }
    }

    public String getMaxDocNo(String strTableName) {
        String str_result = null;
        String str_query = "SELECT  COUNT(*) + 0 AS DOC_NO FROM " + strTableName + "";
        Cursor cu = this.rawQuery(str_query, null);
        if (cu != null && cu.getCount() > 0) {
            cu.moveToFirst();
            str_result = cu.getString(cu.getColumnIndex("DOC_NO"));
        }
        return str_result;
    }

    public void deleteMethod(String ID, String TABLE_NAME, String Column_Name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, Column_Name + " =?",new String[]{ID});
        db.close();
    }


    public void deleteList(String TABLE_NAME) {
        db = this.getWritableDatabase();
//       db.delete(TABLE_NAME,null,null);
        db.execSQL("DELETE FROM "+TABLE_NAME);
    }

    //Double cCom = ((double)npage/numpages) * 100;
    public void updateItem(String TableName, ContentValues values, String product_id, String Column_ID){
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TableName, values, Column_ID + " LIKE ?", new String[] {product_id});
        long rowInserted = db.update(TableName, values, Column_ID + " LIKE ?", new String[] {product_id});
        if (rowInserted != -1) {
            Log.v("Updating..", values + "");
        }
        else{
            Log.v("Failed to Update...", values + "");
        }
    }


    public Boolean  getItemExist(String TABLE_NAME, String Column_Name, String product_id) {
        boolean exist = false;
        String query = " SELECT * FROM " + TABLE_NAME + " Where " + Column_Name + " = ?";

        Cursor cursor = rawQuery(query, new String[]{product_id});
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                exist = true;
//                String id = cursor.getString(cursor.getColumnIndex(Column_Game_Locks));
            } else {
                exist = false;
            }
        } else {
            exist = false;
        }
        return exist;
    }



//    public List<DBModel> TempList() {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        String str_query = "Select * From "+TABLE_NAME;
//        Cursor cursor = db.rawQuery(str_query,null);
//        List<DBModel> Locklist = new ArrayList<DBModel>();
//
//        if (cursor != null && cursor.getCount() > 0 && cursor.isBeforeFirst()) {
//            while (cursor.moveToNext()){
//                DBModel list = new DBModel();
//                list.setID(cursor.getString(cursor.getColumnIndex(Column_ID)));
//                list.setLockId(cursor.getString(cursor.getColumnIndex(Column_Lock_ID)));
//                list.setGameLocks(cursor.getString(cursor.getColumnIndex(Column_Game_Locks)));
//                list.setLockAnswers(cursor.getString(cursor.getColumnIndex(Column_Lock_Answers)));
//                list.setLockIsCompleted(cursor.getString(cursor.getColumnIndex(Column_Lock_isCompleted)));
//                list.setLockCompletedTime(cursor.getString(cursor.getColumnIndex(Column_Lock_Completed_Time)));
//                list.setLockPlayedAnswers(cursor.getString(cursor.getColumnIndex(Column_Lock_Played_Answers)));
//                list.setLockPlayed(cursor.getString(cursor.getColumnIndex(Column_Lock_Played)));
//                list.setLockType(cursor.getString(cursor.getColumnIndex(Column_Lock_Type)));
//
//                Locklist.add(list);
//            }
//        }
//
//        return Locklist;
//
//    }


    public String getTotalPrice (){
        SQLiteDatabase db = this.getWritableDatabase();
        String totalPrice = "";
//        String str_query = "Select * From "+ CartTB + " Where " + Column_Name + " = ?";
        String str_query = "Select SUM(product_price) From "+ CartTB ;
        Cursor cursor = db.rawQuery(str_query, null);
        if(cursor.moveToFirst())
            totalPrice = String.valueOf(cursor.getInt(0));
        else
            totalPrice = String.valueOf(-1);
        return totalPrice;
    }

    public List<CartModel> TempList2() {
        SQLiteDatabase db = this.getWritableDatabase();
//        String str_query = "Select * From "+ CartTB + " Where " + Column_Name + " = ?";
        String str_query = "Select * From "+ CartTB ;
        Cursor cursor = db.rawQuery(str_query, null);
//        Cursor cursor = db.rawQuery(str_query, new String[]{user_id});
        List<CartModel> Locklist = new ArrayList<CartModel>();

        if (cursor != null && cursor.getCount() > 0 && cursor.isBeforeFirst()) {
            while (cursor.moveToNext()){
                CartModel list = new CartModel();
                list.setProduct_ID(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_id))+"");
                list.setProduct_Name(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_name))+"");
                list.setProduct_Price(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_price))+"");
                list.setProduct_Quantity(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_quantity))+"");
                list.setProduct_Tax(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_tax))+"");
                list.setUser_ID(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_id))+"");
                Locklist.add(list);
            }
        }

        return Locklist;

    }

    public List<CartModel> TempList() {
        SQLiteDatabase db = this.getWritableDatabase();
//        String str_query = "Select * From "+ CartTB + " Where " + Column_Name + " = ?";
        String str_query = "Select * From "+ CartTB ;
        Cursor cursor = db.rawQuery(str_query, null);
//        Cursor cursor = db.rawQuery(str_query, new String[]{user_id});
        List<CartModel> Locklist = new ArrayList<CartModel>();

        if (cursor != null && cursor.getCount() > 0 && cursor.isBeforeFirst()) {
            while (cursor.moveToNext()){
                CartModel list = new CartModel();
                list.setProduct_ID(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_id))+"");
                list.setProduct_Name(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_name))+"");
                list.setProduct_Price(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_price))+"");
                list.setProduct_Quantity(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_quantity))+"");
                list.setProduct_Tax(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_tax))+"");
                list.setUser_ID(cursor.getString(cursor.getColumnIndex(DBTablesUtils.product_id))+"");
                Locklist.add(list);
            }
        }

        return Locklist;
    }
}