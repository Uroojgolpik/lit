package lit.com.DataBase.Utils;

import lit.com.DataBase.DBExcutionFile.LITDB;

public class DBTablesUtils {

    public static String product_id = "product_id";
    public static String product_quantity = "product_quantity";
    public static String product_price = "product_price";
    public static String product_tax = "product_tax";
    public static String product_name = "product_name";
    public static String user_id = "user_id";

    public static String token = "token";
    public static String first_name = "first_name";
    public static String last_name = "last_name";
    public static String email = "email";
    public static String country_id = "country_id";
    public static String state = "state";
    public static String city = "city";
    public static String address1 = "address1";
    public static String zip = "zip";
    public static String phone = "phone";
    public static String gender = "gender";
    public static String cc = "cc";
    public static String expMonth = "exp_month";
    public static String expYear = "exp_year";
    public static String cvc = "cvc";
    public static String message = "message";
    public static String grandTotal = "grandTotal";
    public static String address2 = "address2";
    public static String year = "year";
    public static String month = "month";
    public static String date = "date";
    public static String column_id = "column_id";


    public static String CREATE_TABLE_CT = "CREATE TABLE " + LITDB.CartTB + "(" + column_id
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + product_id + " TEXT, " + product_name + " TEXT, " + product_price + " TEXT, " +
            product_quantity + " TEXT, " + product_tax + " TEXT, " + user_id + " TEXT " +");";

    public static String CREATE_TABLE_PT = "CREATE TABLE " + LITDB.PersonalTB + "(" + column_id
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + token + " TEXT," + user_id + " TEXT," + first_name + " TEXT," +
            last_name + " TEXT," + email + " TEXT," + state + " TEXT," + city + " TEXT,"
            + country_id + " TEXT, " + address1 + " TEXT, " + zip + " TEXT, " + phone + " TEXT, "
            + gender + " TEXT," + address2 + " TEXT," + year + " TEXT," + month + " TEXT," + date + " TEXT" +");";

    public static String CREATE_TABLE_COT = "CREATE TABLE " + LITDB.CheckoutTB + "(" + column_id
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + token + " TEXT," + user_id + " TEXT," + first_name + " TEXT," +
            last_name + " TEXT," + email + " TEXT," + state + " TEXT," + city + " TEXT,"
            + country_id + " TEXT," + address1 + " TEXT," + zip + " TEXT," + phone + " TEXT,"
            + gender + " TEXT," + address2 + " TEXT," + year + " TEXT," + month + " TEXT," + date + " TEXT,"
            + product_id + product_name + " TEXT, " + " TEXT," + product_price + " TEXT," + product_quantity + " TEXT," + product_tax
            + " TEXT," + grandTotal + " TEXT," + cc + " TEXT,"
            + expMonth + " TEXT," + expYear + " TEXT," + cvc + " TEXT"+");";
}
