package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTestData {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("sku")
@Expose
private String sku;
@SerializedName("name")
@Expose
private String name;
@SerializedName("teaser")
@Expose
private String teaser;
@SerializedName("price")
@Expose
private Integer price;
@SerializedName("salePrice")
@Expose
private Integer salePrice;
@SerializedName("sale")
@Expose
private Integer sale;
@SerializedName("description")
@Expose
private String description;
@SerializedName("requirments")
@Expose
private String requirments;
@SerializedName("keywords")
@Expose
private String keywords;
@SerializedName("key")
@Expose
private String key;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getSku() {
return sku;
}

public void setSku(String sku) {
this.sku = sku;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getTeaser() {
return teaser;
}

public void setTeaser(String teaser) {
this.teaser = teaser;
}

public Integer getPrice() {
return price;
}

public void setPrice(Integer price) {
this.price = price;
}

public Integer getSalePrice() {
return salePrice;
}

public void setSalePrice(Integer salePrice) {
this.salePrice = salePrice;
}

public Integer getSale() {
return sale;
}

public void setSale(Integer sale) {
this.sale = sale;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getRequirments() {
return requirments;
}

public void setRequirments(String requirments) {
this.requirments = requirments;
}

public String getKeywords() {
return keywords;
}

public void setKeywords(String keywords) {
this.keywords = keywords;
}

public String getKey() {
return key;
}

public void setKey(String key) {
this.key = key;
}

}