
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearByServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<NearByServicesData> data = null;
    @SerializedName("errors")
    @Expose
    private NearByServicesErrors errors;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<NearByServicesData> getData() {
        return data;
    }

    public void setData(List<NearByServicesData> data) {
        this.data = data;
    }

    public NearByServicesErrors getErrors() {
        return errors;
    }

    public void setErrors(NearByServicesErrors errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
