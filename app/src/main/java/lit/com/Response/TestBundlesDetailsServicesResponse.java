
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestBundlesDetailsServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<TestBundlesDetailsServicesData> data = null;
    @SerializedName("errors")
    @Expose
    private TestBundlesDetailsServicesError errors;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TestBundlesDetailsServicesData> getData() {
        return data;
    }

    public void setData(List<TestBundlesDetailsServicesData> data) {
        this.data = data;
    }

    public TestBundlesDetailsServicesError getErrors() {
        return errors;
    }

    public void setErrors(TestBundlesDetailsServicesError errors) {
        this.errors = errors;
    }

}
