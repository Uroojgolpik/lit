
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyOrdersServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<MyOrdersServicesData> data = null;
    @SerializedName("errors")
    @Expose
    private MyOrdersServicesErrors errors;
    @SerializedName("message")
    @Expose
    private String message;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<MyOrdersServicesData> getData() {
        return data;
    }

    public void setData(List<MyOrdersServicesData> data) {
        this.data = data;
    }

    public MyOrdersServicesErrors getErrors() {
        return errors;
    }

    public void setErrors(MyOrdersServicesErrors errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
