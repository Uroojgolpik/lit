
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<TestServicesData> data = null;
    @SerializedName("errors")
    @Expose
    private TestServicesError errors;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TestServicesData> getData() {
        return data;
    }

    public void setData(List<TestServicesData> data) {
        this.data = data;
    }

    public TestServicesError getErrors() {
        return errors;
    }

    public void setErrors(TestServicesError errors) {
        this.errors = errors;
    }

}
