
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateServicesResponse {

    @SerializedName("data")
    @Expose
    private List<StateServicesData> data = null;

    public List<StateServicesData> getData() {
        return data;
    }

    public void setData(List<StateServicesData> data) {
        this.data = data;
    }

}
