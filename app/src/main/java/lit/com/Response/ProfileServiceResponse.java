
package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileServiceResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private ProfileServiceData data;
    @SerializedName("errors")
    @Expose
    private ProfileServiceErrors errors;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ProfileServiceData getData() {
        return data;
    }

    public void setData(ProfileServiceData data) {
        this.data = data;
    }

    public ProfileServiceErrors getErrors() {
        return errors;
    }

    public void setErrors(ProfileServiceErrors errors) {
        this.errors = errors;
    }

}
