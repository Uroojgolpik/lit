
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestBundlesServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<TestBundlesServicesData> data = null;
    @SerializedName("errors")
    @Expose
    private TestBundlesServicesError errors;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("tax")
    @Expose
    private List<TestTaxServiceData> tax = null;
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TestBundlesServicesData> getData() {
        return data;
    }

    public void setData(List<TestBundlesServicesData> data) {
        this.data = data;
    }

    public TestBundlesServicesError getErrors() {
        return errors;
    }

    public void setErrors(TestBundlesServicesError errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TestTaxServiceData> getTax() {
        return tax;
    }

    public void setTax(List<TestTaxServiceData> tax) {
        this.tax = tax;
    }
}
