
package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignupServiceResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private SignupServiceData data;
    @SerializedName("errors")
    @Expose
    private SignupServiceErrors errors;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SignupServiceData getData() {
        return data;
    }

    public void setData(SignupServiceData data) {
        this.data = data;
    }

    public SignupServiceErrors getErrors() {
        return errors;
    }

    public void setErrors(SignupServiceErrors errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
