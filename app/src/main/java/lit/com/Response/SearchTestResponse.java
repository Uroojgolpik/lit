package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchTestResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("data")
@Expose
private List<SearchTestData> data = null;
@SerializedName("errors")
@Expose
private SearchTestErrors errors;
@SerializedName("message")
@Expose
private String message;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public List<SearchTestData> getData() {
return data;
}

public void setData(List<SearchTestData> data) {
this.data = data;
}

public SearchTestErrors getErrors() {
return errors;
}

public void setErrors(SearchTestErrors errors) {
this.errors = errors;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}