
package lit.com.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestServiceResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<TestServiceData> data = null;
    @SerializedName("errors")
    @Expose
    private TestServiceError errors;

    public Integer getStatus() {
        return status;
    }
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("tax")
    @Expose
    private List<TestTaxServiceData> tax = null;
    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TestServiceData> getData() {
        return data;
    }

    public void setData(List<TestServiceData> data) {
        this.data = data;
    }

    public TestServiceError getErrors() {
        return errors;
    }

    public void setErrors(TestServiceError errors) {
        this.errors = errors;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<TestTaxServiceData> getTax() {
        return tax;
    }

    public void setTax(List<TestTaxServiceData> tax) {
        this.tax = tax;
    }


}
