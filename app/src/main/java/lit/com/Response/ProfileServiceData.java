
package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileServiceData {

    @SerializedName("user")
    @Expose
    private ProfileServiceDataUser user;
    @SerializedName("address")
    @Expose
    private ProfileServiceDataAddress address;

    public ProfileServiceDataUser getUser() {
        return user;
    }

    public void setUser(ProfileServiceDataUser user) {
        this.user = user;
    }

    public ProfileServiceDataAddress getAddress() {
        return address;
    }

    public void setAddress(ProfileServiceDataAddress address) {
        this.address = address;
    }

}
