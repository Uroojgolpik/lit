
package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestDetailsServicesResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private TestDetailsServicesData data;
    @SerializedName("errors")
    @Expose
    private TestDetailsServicesError errors;
    @SerializedName("message")
    @Expose
    private String message;
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public TestDetailsServicesData getData() {
        return data;
    }

    public void setData(TestDetailsServicesData data) {
        this.data = data;
    }

    public TestDetailsServicesError getErrors() {
        return errors;
    }

    public void setErrors(TestDetailsServicesError errors) {
        this.errors = errors;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
