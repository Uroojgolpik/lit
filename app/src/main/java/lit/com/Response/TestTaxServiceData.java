package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestTaxServiceData {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("teaser")
@Expose
private Object teaser;
@SerializedName("description")
@Expose
private String description;
@SerializedName("image")
@Expose
private Object image;
@SerializedName("keywords")
@Expose
private Object keywords;
@SerializedName("status")
@Expose
private Integer status;
@SerializedName("deleted")
@Expose
private Integer deleted;
@SerializedName("created_at")
@Expose
private Object createdAt;
@SerializedName("updated_at")
@Expose
private Object updatedAt;
@SerializedName("url")
@Expose
private Object url;
@SerializedName("category_id")
@Expose
private Integer categoryId;
@SerializedName("price")
@Expose
private Integer price;
@SerializedName("sale")
@Expose
private Integer sale;
@SerializedName("salePrice")
@Expose
private Object salePrice;
@SerializedName("priceForDoctors")
@Expose
private Integer priceForDoctors;
@SerializedName("availabeInColors")
@Expose
private Integer availabeInColors;
@SerializedName("quantity")
@Expose
private Integer quantity;
@SerializedName("inStock")
@Expose
private Integer inStock;
@SerializedName("tax")
@Expose
private Object tax;
@SerializedName("type")
@Expose
private String type;
@SerializedName("metaTitle")
@Expose
private Object metaTitle;
@SerializedName("wholeSale")
@Expose
private Integer wholeSale;
@SerializedName("featured")
@Expose
private Object featured;
@SerializedName("inventory")
@Expose
private Object inventory;
@SerializedName("catalogNo")
@Expose
private Object catalogNo;
@SerializedName("isMandatory")
@Expose
private Integer isMandatory;
@SerializedName("sku")
@Expose
private String sku;
@SerializedName("requirments")
@Expose
private String requirments;
@SerializedName("isfeatured")
@Expose
private Integer isfeatured;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Object getTeaser() {
return teaser;
}

public void setTeaser(Object teaser) {
this.teaser = teaser;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public Object getImage() {
return image;
}

public void setImage(Object image) {
this.image = image;
}

public Object getKeywords() {
return keywords;
}

public void setKeywords(Object keywords) {
this.keywords = keywords;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public Integer getDeleted() {
return deleted;
}

public void setDeleted(Integer deleted) {
this.deleted = deleted;
}

public Object getCreatedAt() {
return createdAt;
}

public void setCreatedAt(Object createdAt) {
this.createdAt = createdAt;
}

public Object getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(Object updatedAt) {
this.updatedAt = updatedAt;
}

public Object getUrl() {
return url;
}

public void setUrl(Object url) {
this.url = url;
}

public Integer getCategoryId() {
return categoryId;
}

public void setCategoryId(Integer categoryId) {
this.categoryId = categoryId;
}

public Integer getPrice() {
return price;
}

public void setPrice(Integer price) {
this.price = price;
}

public Integer getSale() {
return sale;
}

public void setSale(Integer sale) {
this.sale = sale;
}

public Object getSalePrice() {
return salePrice;
}

public void setSalePrice(Object salePrice) {
this.salePrice = salePrice;
}

public Integer getPriceForDoctors() {
return priceForDoctors;
}

public void setPriceForDoctors(Integer priceForDoctors) {
this.priceForDoctors = priceForDoctors;
}

public Integer getAvailabeInColors() {
return availabeInColors;
}

public void setAvailabeInColors(Integer availabeInColors) {
this.availabeInColors = availabeInColors;
}

public Integer getQuantity() {
return quantity;
}

public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

public Integer getInStock() {
return inStock;
}

public void setInStock(Integer inStock) {
this.inStock = inStock;
}

public Object getTax() {
return tax;
}

public void setTax(Object tax) {
this.tax = tax;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

public Object getMetaTitle() {
return metaTitle;
}

public void setMetaTitle(Object metaTitle) {
this.metaTitle = metaTitle;
}

public Integer getWholeSale() {
return wholeSale;
}

public void setWholeSale(Integer wholeSale) {
this.wholeSale = wholeSale;
}

public Object getFeatured() {
return featured;
}

public void setFeatured(Object featured) {
this.featured = featured;
}

public Object getInventory() {
return inventory;
}

public void setInventory(Object inventory) {
this.inventory = inventory;
}

public Object getCatalogNo() {
return catalogNo;
}

public void setCatalogNo(Object catalogNo) {
this.catalogNo = catalogNo;
}

public Integer getIsMandatory() {
return isMandatory;
}

public void setIsMandatory(Integer isMandatory) {
this.isMandatory = isMandatory;
}

public String getSku() {
return sku;
}

public void setSku(String sku) {
this.sku = sku;
}

public String getRequirments() {
return requirments;
}

public void setRequirments(String requirments) {
this.requirments = requirments;
}

public Integer getIsfeatured() {
return isfeatured;
}

public void setIsfeatured(Integer isfeatured) {
this.isfeatured = isfeatured;
}

}