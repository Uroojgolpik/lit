
package lit.com.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileViewResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private ProfileViewData data;
    @SerializedName("errors")
    @Expose
    private ProfileViewError errors;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ProfileViewData getData() {
        return data;
    }

    public void setData(ProfileViewData data) {
        this.data = data;
    }

    public ProfileViewError getErrors() {
        return errors;
    }

    public void setErrors(ProfileViewError errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
