package lit.com.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Activities.DashBoardActivity;
import lit.com.DataBase.DBExcutionFile.LITDB;
import lit.com.DataBase.Models.CartModel;
import lit.com.R;
import lit.com.Response.CheckoutServiceResponse;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.SnackBarUtils;
import lit.com.UTILS.UserSession;
import lit.com.UTILS.Validations;
import lit.com.UTILS.Validator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmenCheckoutCreditCard extends Fragment  {


    View view;
    EditText edt_cc, edt_cvv;
    Spinner sp_month, sp_year, sp_card_type;
    UserSession userSession;
    String token, email, firstName, lastName, dob, gender, state, city, phone, zip, address,
            country, cc, cvv, exp_month, exp_year, cc_type, product_id, price, grandTotal,
            message, tax;
    String TAG = "FragmenCheckoutCreditCard";;
    String date [];
    LinearLayout ll_done, ll_back;
    LITServices services;
    DialogBoxes dialogBoxes;
    Bundle bundle ;
    AlertDialog alertDialog1;
    byte type = 10;
    UserSession session;
    ArrayList<String> years;
    static Integer selectedMonth, selectedYear, selectedCardType;
    ArrayAdapter monthAdapter, yearAdapter, cardAdapter;
    String [] months  = {"Select A Month","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    String [] cardArray = {"Select Card Type", "VISA", "MASTER"};
    List<CartModel> datalist = new ArrayList<>();
    ImageButton ib_card_info;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        firstName= bundle.getString("fn");
        lastName= bundle.getString("ln");
        email = bundle.getString("email" );
        gender = bundle.getString("gender" );
        dob = bundle.getString("dob");
        date = dob.split("-");
        state = bundle.getString("state" );
        phone = bundle.getString("phone" );
        zip = bundle.getString("zip");
        address = bundle.getString("address" );
        country = bundle.getString("country" );
        city = bundle.getString("city" );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedtInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkout_cardinfo, container, false);
        getUserSession();
        linkXML(view);
        return view;
    }

    private void linkXML(View view) {

        edt_cc = (EditText) view.findViewById(R.id.checkout_edt_cc);
        sp_month = (Spinner) view.findViewById(R.id.checkout_sp_expire_month);
        sp_year = (Spinner) view.findViewById(R.id.checkout_sp_expire_year);
        sp_card_type = (Spinner) view.findViewById(R.id.checkout_sp_card_type);
        edt_cvv = (EditText) view.findViewById(R.id.checkout_edt_cvv);
        ib_card_info = (ImageButton) view.findViewById(R.id.ib_card_info);
        ll_done = (LinearLayout) view.findViewById(R.id.checkout_ll_done);
        ll_back = (LinearLayout) view.findViewById(R.id.checkout_ll_back);
        ll_back.setVisibility(View.GONE);

        getYears();
        getMonthAdapter();
        getCardAdapter();
        onClickListner();

    }

    private void getMonthAdapter() {

        monthAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, months);
        sp_month.setAdapter(monthAdapter);
    }
    private void getCardAdapter() {

        cardAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, cardArray);
        sp_card_type.setAdapter(cardAdapter);
    }

    private void onClickListner() {

        ll_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              verifyAll();
            }
        });
        ib_card_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getFragmentManager().popBackStack();
            }
        });

        sp_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    exp_month = String.valueOf(months[position]);
                }
                ((TextView) view).setTextColor(Color.BLACK);
                selectedMonth = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    exp_year = String.valueOf(years.get(position));
                }

                ((TextView) view).setTextColor(Color.BLACK);
                selectedYear = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_card_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){
                    cc_type = String.valueOf(cardArray[position]);
                    if (cc_type.equals("VISA")){
                        type = 0;
                    }

                    if (cc_type.equals("MASTER")){
                        type = 1;
                    }




                }

                ((TextView) view).setTextColor(Color.BLACK);
                selectedCardType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void verifyAll() {

//        exp_month = sp_month.getSelectedItem().toString();
//        exp_year = sp_year.getSelectedItem().toString();
        cc = edt_cc.getText().toString();
        cvv = edt_cvv.getText().toString();

        if (TextUtils.isEmpty(cc_type)){
            DialogBoxes.alterDialog(getActivity(), "Select Card Type");
        }

        else if (TextUtils.isEmpty(cc)){
            edt_cc.setError("Enter Your Card Number");
        }


        else if (TextUtils.isEmpty(exp_month)){
            DialogBoxes.alterDialog(getActivity(), "Select Expiration Month");
        }
        else if (TextUtils.isEmpty(exp_year)){
            DialogBoxes.alterDialog(getActivity(), "Select Expiration Expiration Year");
        }


        else if (Validations.cvvValidator(cvv)){
            edt_cvv.setError("Enter Your CVV");
        }

        else {

            if (!Validator.validate(cc, type)){
                DialogBoxes.alterDialog(getActivity(), "Invalid Card number");
            }
            else {
                if (getCartData())
                {
                    LITDB db = new LITDB(getActivity());
                    price = getPrice().toString().replace("[","").replace("]","");
                    product_id = getProductIds().toString().replace("[","").replace("]","");
                    grandTotal = db.getTotalPrice()+"";
                    cc = "4242424242424242";
                    cvv = "1234";

                    Log.d(TAG, "verifyAll: "+grandTotal);
                    Log.d(TAG, "verifyAll: "+product_id);
                    Log.d(TAG, "verifyAll: "+price);
                    executeService();
                }
            }
        }
    }

    public void alterDialog (final String text){
        alertDialog1 = new AlertDialog.Builder(getActivity()).create();
        alertDialog1.setMessage(text);

        alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LITDB litdb = new LITDB(getActivity());
                litdb.deleteList(LITDB.CartTB);
                alertDialog1.dismiss();
                startActivity(new Intent(getActivity(), DashBoardActivity.class));
                getActivity().finishAffinity();
            }
        });

        alertDialog1.show();
    }

    public void getYears(){
        years = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        years.add("Select A Year");

        for (int i=0; i<=15; i++){
            years.add(String.valueOf(year+i));
        }

        yearAdapter  = new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, years);
        sp_year.setAdapter(yearAdapter);
        Log.d("getYears", "getYears: "+years);
    }





    private void executeService() {

        dialogBoxes = new DialogBoxes(getActivity());
        dialogBoxes.showProgress();
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<CheckoutServiceResponse> executeService = services.checkout(token,
                firstName, lastName, email, country, state,
                city, address, zip,phone, gender, cc, exp_month,
                exp_year, cvv, product_id, price,
                message, grandTotal, address, address, date[0], date[1], date[2]);

        executeService.enqueue(new Callback<CheckoutServiceResponse>() {
            @Override
            public void onResponse(Call<CheckoutServiceResponse> call, Response<CheckoutServiceResponse> response) {

                if (response.isSuccessful()) {
//                    Toast.makeText(getActivity(), "server returned so many repositories: " +
//                            response.body().getData(), Toast.LENGTH_SHORT).show();
                        if (response.body().getStatus().equals(0)){
                            if (response.body().getData() != null){
                                dialogBoxes.hideProgress();
                                alterDialog(response.body().getMessage());
                            }
                        }
                        else {
                            dialogBoxes.hideProgress();
                            DialogBoxes.alterDialog(getActivity(), response.body().getMessage());
                        }


                }
                else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Toast.makeText(getActivity(), "not found", Toast.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Toast.makeText(getActivity(), "server broken", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(getActivity(), "unknown error", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    dialogBoxes.hideProgress();
                }











            }

            @Override
            public void onFailure(Call<CheckoutServiceResponse> call, Throwable t) {
                SnackBarUtils.showSnackBar(getActivity().findViewById(android.R.id.content),
                        "Something Went Wrong, Try Again",Snackbar.LENGTH_LONG);
                dialogBoxes.hideProgress();

            }
        });
    }

    private void getUserSession(){
        userSession = new UserSession(getActivity());
        HashMap<String, String> userSessionDetails = userSession.getUserDetails();
        token = userSessionDetails.get(UserSession.APITOKEN);
        Log.d("APITOKEN", ""+ String.valueOf(userSessionDetails.get(UserSession.APITOKEN)));
    }
    private Boolean getCartData(){
        boolean isData = false;
        LITDB db = new LITDB(getActivity());
        datalist = db.TempList();
        if (datalist.size()>0){
            isData = true;
            getProductIds();
            getPrice();

        }
        return isData;

    }

    private ArrayList getPrice() {
        ArrayList<String> array = new ArrayList<>();
        for (int i = 0; i < datalist.size(); i++){
            array.add(datalist.get(i).getProduct_Price());
        }
        return array;
    }

    private ArrayList getProductIds() {
        ArrayList<String> array = new ArrayList<>();
        for (int i = 0; i < datalist.size(); i++){
            array.add(datalist.get(i).getProduct_ID());
        }
        return array;
    }
}

