package lit.com.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.HashMap;

import lit.com.R;
import lit.com.Response.User;
import lit.com.UTILS.DatePicker;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.UserSession;
import lit.com.UTILS.Validations;


public class FragmentCheckoutPersonalInfo extends Fragment {


    View view;
    EditText edt_email, edt_firstName, edt_lastName, edt_phone;
    String email, firstName, lastName, date = null, gender=null, phone;
    RadioButton rb_male, rb_female;
    TextView tv_dob;
    LinearLayout ll_next;
    ImageButton ib_personal_info;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedtInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkout_personal, container, false);
        linkXML(view);
        return view;
    }

    private void linkXML(View view) {
        
        edt_firstName = (EditText) view.findViewById(R.id.checkout_edt_fn);
        edt_lastName = (EditText) view.findViewById(R.id.checkout_edt_ln);
        edt_email = (EditText) view.findViewById(R.id.checkout_edt_email);
        edt_phone = (EditText) view.findViewById(R.id.checkout_edt_phone);
        tv_dob = (TextView) view.findViewById(R.id.checkout_tv_dob);
        rb_male = (RadioButton) view.findViewById(R.id.checkout_rb_male);
        rb_female = (RadioButton) view.findViewById(R.id.checkout_rb_female);
        ll_next = (LinearLayout) view.findViewById(R.id.checkout_ll_next);
        ib_personal_info = (ImageButton) view.findViewById(R.id.ib_personal_info);

        getUserSession();
        onClickListner();
    }

    private void onClickListner() {

        tv_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker datePicker = new DatePicker(getActivity());
                datePicker.datepicker(tv_dob);

            }
        });

        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyAll();
            }
        });

        ib_personal_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

    }

    private void verifyAll() {
        email = edt_email.getText().toString();
        firstName = edt_firstName.getText().toString();
        lastName = edt_lastName.getText().toString();
        phone = edt_phone.getText().toString();

        if (rb_male.isChecked()){
            gender = rb_male.getText().toString();
        }
        if (rb_female.isChecked()){
            gender = rb_female.getText().toString();
        }

        if (!Validations.firstLastName(firstName)){
            edt_firstName.setError("Enter Your First Name");
        }
        else if (!Validations.firstLastName(lastName)){
            edt_lastName.setError("Enter Your Last Name");
        }
        else if (!Validations.isEmailValid(email)){
            edt_email.setError("Enter Your Email");
        }
        else if (!Validations.number(phone)){
            edt_phone.setError("Enter Your Phone Number");
        }
        else if (TextUtils.isEmpty(tv_dob.getText().toString())){
            DialogBoxes.alterDialog(getActivity(), "Select Date Of Birth");
        }
        else if (TextUtils.isEmpty(gender)){
            DialogBoxes.alterDialog(getActivity(), "Select Gender");
        }
        else {
//            date = DatePicker.date;
            Log.v("date: ", date+"");
            Log.v("date: ", DatePicker.date+"");
            Bundle bundle = new Bundle();
            Fragment fragment = new FragmenCheckoutAddressInfo();

            bundle.putString("fn", firstName);
            bundle.putString("ln", lastName);
            bundle.putString("email", email);
            bundle.putString("gender", gender);
            bundle.putString("dob", date);
            bundle.putString("phone", phone);
            fragment.setArguments(bundle);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_checkout, fragment, "FragmenCheckoutAddressInfo")
                    .addToBackStack("FragmenCheckoutAddressInfo")
                    .commitAllowingStateLoss();
        }


    }


    public void getUserSession() {
        UserSession userSession = new UserSession(getActivity());
        HashMap<String, String> getData = userSession.getUserDetails();
        if (!userSession.checkLogin()) {
            String username = getData.get(UserSession.USERNAME);
            String[] array = username.split(" ");
            firstName = array[0];
            lastName = array[0];
            email = getData.get(UserSession.EMAIL);
            date = getData.get(UserSession.DOB);
            phone = getData.get(UserSession.PHONE);
            gender = getData.get(UserSession.GENDER);

            edt_firstName.setText(firstName);
            edt_lastName.setText(lastName);
            edt_email.setText(email);
            tv_dob.setText(date);
            edt_phone.setText(phone);
            if (gender.equals("Female") || gender.equals("female")) {
                rb_female.setChecked(true);
                rb_male.setChecked(false);
            }
            if (gender.equals("Male") || gender.equals("male")) {
                rb_male.setChecked(true);
                rb_female.setChecked(false);
            }
        }
    }

}
