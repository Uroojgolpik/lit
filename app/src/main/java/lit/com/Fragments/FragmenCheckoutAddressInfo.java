package lit.com.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lit.com.APIs.LITServices;
import lit.com.APIs.RetrofitClient;
import lit.com.Adapter.CountrySpinnerAdapter;
import lit.com.Adapter.SpinnerAdapter;
import lit.com.R;
import lit.com.Response.CountryData;
import lit.com.Response.CountryResponse;
import lit.com.Response.StateServicesData;
import lit.com.Response.StateServicesResponse;
import lit.com.UTILS.DatePicker;
import lit.com.UTILS.DialogBoxes;
import lit.com.UTILS.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmenCheckoutAddressInfo extends Fragment  {


    View view;
    EditText  edt_city, edt_phone, edt_zip_code, edt_address;
    Spinner sp_state, sp_country;
    String email, firstName, lastName, dob, gender, state, city, phone, zip, address, country;
    String date [];
    LinearLayout ll_next, ll_back;
    LITServices services;
    Bundle bundle ;
    AlertDialog alertDialog1;
    ImageButton ib_address_info;
    List<StateServicesData> dataListState = new ArrayList<StateServicesData>();
    List<CountryData> dataListCountry = new ArrayList<CountryData>();



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
        firstName= bundle.getString("fn");
        lastName= bundle.getString("ln");
        email = bundle.getString("email" );
        phone = bundle.getString("phone");
        gender = bundle.getString("gender" );
        dob = bundle.getString("dob");
        date = dob.split("-");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedtInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkout_addresss, container, false);
        linkXML(view);

        return view;
    }

    private void linkXML(View view) {

        edt_city = (EditText) view.findViewById(R.id.checkout_edt_city);

        sp_state = (Spinner) view.findViewById(R.id.checkout_sp_state);
        sp_country = (Spinner) view.findViewById(R.id.checkout_sp_country);
        edt_zip_code = (EditText) view.findViewById(R.id.checkout_edt_postal_code);
        edt_address = (EditText) view.findViewById(R.id.checkout_edt_address);
        ib_address_info = (ImageButton) view.findViewById(R.id.ib_address_info);
        ll_next = (LinearLayout) view.findViewById(R.id.checkout_ll_next);
        ll_back = (LinearLayout) view.findViewById(R.id.checkout_ll_back);
        ll_back.setVisibility(View.GONE);

        if (dataListState.size()<=0){
            getStateList();
        }
        else {
            if (dataListState.size()>0) {
                SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, dataListState);
                sp_state.setAdapter(spinnerAdapter);
            }
        }
        if (dataListCountry.size()<=0) {
            getCountryList();
        }
        else {
            if (dataListState.size()>0) {
                CountrySpinnerAdapter spinnerAdapter =
                        new CountrySpinnerAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, dataListCountry);
                sp_country.setAdapter(spinnerAdapter);
            }
        }
        onClickListner();
    }

    private void onClickListner() {

        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              verifyAll();
            }
        });
        ib_address_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                tablayout.getTabAt(0);
                getActivity().getFragmentManager().popBackStack();
            }
        });

        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    state = String.valueOf(dataListState.get(sp_state.getSelectedItemPosition()-1).getCode());
                }
                if (view != null) {
                    ((TextView) view).setTextColor(Color.BLACK);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position>0){

                    country = String.valueOf(dataListCountry.get(sp_country.getSelectedItemPosition()-1).getId());
                }

                if (view != null) {
                    ((TextView) view).setTextColor(Color.BLACK);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void verifyAll() {

        //state = sp_state.getSelectedItem().toString();
        city = edt_city.getText().toString();
        zip = edt_zip_code.getText().toString();
        address = edt_address.getText().toString();

        if (TextUtils.isEmpty(state)){
            DialogBoxes.alterDialog(getActivity(), "Select Any State");
        }
        else if (TextUtils.isEmpty(city)){
            edt_city.setError("Enter Your City");
        }

        else if (TextUtils.isEmpty(zip)){
            edt_zip_code.setError("Enter Your Postal Code");
        }

        else if (TextUtils.isEmpty(address)){
            edt_address.setError("Enter Your ProfileServiceDataAddress");
        }
        else {

            Log.v("date: ", date+"");
            Log.v("date: ", DatePicker.date+"");
            Bundle bundle = new Bundle();
            Fragment fragment = new FragmenCheckoutCreditCard();

            bundle.putString("fn", firstName);
            bundle.putString("ln", lastName);
            bundle.putString("email", email);
            bundle.putString("gender", gender);
            bundle.putString("dob", dob);
            bundle.putString("state", state);
            bundle.putString("phone", phone);
            bundle.putString("zip", zip);
            bundle.putString("address", address);
            bundle.putString("country", country);
            bundle.putString("city", city);
            fragment.setArguments(bundle);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_checkout, fragment, "FragmenCheckoutCreditCard")
                    .addToBackStack("FragmenCheckoutCreditCard")
                    .commitAllowingStateLoss();

        }
    }



    public void alterDialog (final String text){
        alertDialog1 = new AlertDialog.Builder(getActivity()).create();
        alertDialog1.setMessage(text);

        alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog1.dismiss();
                getActivity().finish();
            }
        });


        alertDialog1.show();
    }

    private void getStateList() {
        final StateServicesData stateServicesData = new StateServicesData("Select A State", "");
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<StateServicesResponse> getStates = services.getStates();
        getStates.enqueue(new Callback<StateServicesResponse>() {
            @Override
            public void onResponse(Call<StateServicesResponse> call, Response<StateServicesResponse> response) {
                Gson gson = new Gson();
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataListState.add(stateServicesData);
                        dataListState.addAll(response.body().getData());
                        if (dataListState.size()>0) {
                            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(getActivity(),
                                    android.R.layout.simple_dropdown_item_1line, dataListState);
                            sp_state.setAdapter(spinnerAdapter);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<StateServicesResponse> call, Throwable t) {

            }
        });
    }

    private void getCountryList() {
        final CountryData countryData = new CountryData("Select A Country", 0);
        services = RetrofitClient.retrofit.create(LITServices.class);
        Call<CountryResponse> getStates = services.getCountry();
        getStates.enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                Gson gson = new Gson();
                assert response.body() != null;
                if (response.body().getData()!=null){
                    if (response.body().getData().size()>0){
                        dataListCountry.add(countryData);
                        dataListCountry.addAll(response.body().getData());
                        if (dataListState.size()>0) {
                            CountrySpinnerAdapter spinnerAdapter =
                                    new CountrySpinnerAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, dataListCountry);
                            sp_country.setAdapter(spinnerAdapter);
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {

            }
        });
    }

}
